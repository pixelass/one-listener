# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.5.1"></a>
## [0.5.1](https://github.com/pixelass/one-listener/compare/v0.5.0...v0.5.1) (2016-09-10)


### Bug Fixes

* **release:** don't verify ([edca841](https://github.com/pixelass/one-listener/commit/edca841))



<a name="0.5.0"></a>
# [0.5.0](https://github.com/pixelass/one-listener/compare/v0.4.16...v0.5.0) (2016-07-12)


### Features

* **touch:** added touch listeners ([7e05906](https://github.com/pixelass/one-listener/commit/7e05906))



<a name="0.4.16"></a>
## [0.4.16](https://github.com/pixelass/one-listener/compare/v0.4.15...v0.4.16) (2016-06-28)


### Bug Fixes

* **logs:** remove console.log ([930a313](https://github.com/pixelass/one-listener/commit/930a313))



<a name="0.4.15"></a>
## [0.4.15](https://github.com/pixelass/one-listener/compare/v0.4.14...v0.4.15) (2016-06-28)


### Bug Fixes

* **throttle:** fix throttling ([1defb14](https://github.com/pixelass/one-listener/commit/1defb14))



<a name="0.4.14"></a>
## [0.4.14](https://github.com/pixelass/one-listener/compare/v0.4.12...v0.4.14) (2016-06-28)


### Bug Fixes

* **tests:** fix travis build ([28f7d01](https://github.com/pixelass/one-listener/commit/28f7d01))



<a name="0.4.2"></a>
## [0.4.2](https://github.com/pixelass/one-listener/compare/v0.4.12...v0.4.2) (2016-06-28)


### Bug Fixes

* **tests:** fix travis build ([28f7d01](https://github.com/pixelass/one-listener/commit/28f7d01))



<a name="0.4.12"></a>
## [0.4.12](https://github.com/pixelass/one-listener/compare/v0.4.11...v0.4.12) (2016-06-28)


### Bug Fixes

* **package:** fixes typos ([ae343ca](https://github.com/pixelass/one-listener/commit/ae343ca))



<a name="0.4.11"></a>
## [0.4.11](https://github.com/pixelass/one-listener/compare/v0.4.10...v0.4.11) (2016-06-28)


### Bug Fixes

* **package:** add index.js ([795bcd8](https://github.com/pixelass/one-listener/commit/795bcd8))



<a name="0.4.10"></a>
## [0.4.10](https://github.com/pixelass/one-listener/compare/v0.4.9...v0.4.10) (2016-06-26)



<a name="0.4.9"></a>
## 0.4.9 (2016-06-26)
