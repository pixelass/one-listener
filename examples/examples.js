(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _src = require('../src/');

var _src2 = _interopRequireDefault(_src);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var one = new _src2.default({
  limit: 6,
  throttle: 200
});
var never = new _src2.default({
  limit: Infinity
});
var always = new _src2.default({
  limit: 0,
  throttle: 100
});
var requestEventListener = one.requestEventListener;
var cancelEventListener = one.cancelEventListener;

console.log(one, cancelEventListener);

// request mousemove
var stopMoveTracking = requestEventListener('mousemove', function (e) {
  console.log({
    x: e.pageX,
    y: e.pageY
  });
});

var move = true;
// request scroll
// and cancel mousemove on condition
var trackScroll = function trackScroll(e) {
  console.log(window.scrollY);
  if (window.scrollY > 100) {
    if (move) {
      console.log('mousemove canceled');
      stopMoveTracking();
      move = false;
    }
  }
};

var noop = function noop() {};
requestEventListener('scroll', trackScroll);

for (var i = 0; i < 8; i++) {
  requestEventListener('scroll', noop);
}

for (var _i = 0; _i < 3; _i++) {
  requestEventListener('resize', noop);
}

requestEventListener('resize', function (e) {
  console.log('resizing window');
});

setTimeout(function () {
  one.throttle = 1000;
  one.limit = 30;
  console.log('events throttled');
  setTimeout(function () {
    cancelEventListener('scroll', trackScroll);
    one.throttle = 5000;
    one.limit = 2;
    console.log('events throttled less');
    console.log('scrolling canceled');
    console.log('try resizing the browser (' + one.throttle + 'ms throttle)');
  }, 3000);
}, 3000);

console.log('limit: ', one.limit);
console.log('throttle: ', one.throttle);
console.log('Debugger: \n', one.debug);

never.requestEventListener('scroll', function (e) {
  console.log('never throttled');
});
always.requestEventListener('scroll', function (e) {
  console.log('always throttled by ' + always.throttle + 'ms');
});

},{"../src/":6}],2:[function(require,module,exports){
/* eslint-disable no-undefined,no-param-reassign,no-shadow */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 *
 * @param  {Number}    delay          A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
 * @param  {Boolean}   noTrailing     Optional, defaults to false. If noTrailing is true, callback will only execute every `delay` milliseconds while the
 *                                    throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
 *                                    after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
 *                                    the internal counter is reset)
 * @param  {Function}  callback       A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
 *                                    to `callback` when the throttled-function is executed.
 * @param  {Boolean}   debounceMode   If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms. If `debounceMode` is false (at end),
 *                                    schedule `callback` to execute after `delay` ms.
 *
 * @return {Function}  A new, throttled, function.
 */
module.exports = function ( delay, noTrailing, callback, debounceMode ) {

	// After wrapper has stopped being called, this timeout ensures that
	// `callback` is executed at the proper times in `throttle` and `end`
	// debounce modes.
	var timeoutID;

	// Keep track of the last time `callback` was executed.
	var lastExec = 0;

	// `noTrailing` defaults to falsy.
	if ( typeof noTrailing !== 'boolean' ) {
		debounceMode = callback;
		callback = noTrailing;
		noTrailing = undefined;
	}

	// The `wrapper` function encapsulates all of the throttling / debouncing
	// functionality and when executed will limit the rate at which `callback`
	// is executed.
	function wrapper () {

		var self = this;
		var elapsed = Number(new Date()) - lastExec;
		var args = arguments;

		// Execute `callback` and update the `lastExec` timestamp.
		function exec () {
			lastExec = Number(new Date());
			callback.apply(self, args);
		}

		// If `debounceMode` is true (at begin) this is used to clear the flag
		// to allow future `callback` executions.
		function clear () {
			timeoutID = undefined;
		}

		if ( debounceMode && !timeoutID ) {
			// Since `wrapper` is being called for the first time and
			// `debounceMode` is true (at begin), execute `callback`.
			exec();
		}

		// Clear any existing timeout.
		if ( timeoutID ) {
			clearTimeout(timeoutID);
		}

		if ( debounceMode === undefined && elapsed > delay ) {
			// In throttle mode, if `delay` time has been exceeded, execute
			// `callback`.
			exec();

		} else if ( noTrailing !== true ) {
			// In trailing throttle mode, since `delay` time has not been
			// exceeded, schedule `callback` to execute `delay` ms after most
			// recent execution.
			//
			// If `debounceMode` is true (at begin), schedule `clear` to execute
			// after `delay` ms.
			//
			// If `debounceMode` is false (at end), schedule `callback` to
			// execute after `delay` ms.
			timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
		}

	}

	// Return the wrapper function.
	return wrapper;

};

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _throttle = require('throttle-debounce/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * add the propper event listener to `window`
 * @type {Function}
 * @const
 * @private
 * @param  {String} event - event to listen to
 * @param  {Object} handler - the handler object containing the throttled and unthrottled handlers
 * @param  {Function} handler.default - default handler
 * @param  {Function} handler.throttle - throttled handler
 * @param  {Number} delay - throttle in milliseconds
 * @return {Object} returns the new handlers
 */
var addEventListeners = function addEventListeners(event, handler) {
  var delay = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];

  if (delay > 0) {
    handler.throttle = (0, _throttle2.default)(delay, function (e) {
      return handler.default(e);
    });
    window.addEventListener(event, handler.throttle);
  } else {
    window.addEventListener(event, handler.default);
  }
  return handler;
}; /**
    * Helper function to add eventListeners to `window`
    * @module  addEventListeners
    * @private
    * @author  Gregor Adams  <greg@pixelass.com>
    */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 * @author niksy
 * @private
 * @type {Function}
 * @const
 * @param  {Number} delay - A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250
 *                        (or even higher) are most useful.
 * @param  {Boolean} noTrailing - Optional, defaults to false. If noTrailing is true, callback will only execute every
 *                              `delay` milliseconds while the throttled-function is being called. If noTrailing is false
 *                              or unspecified, callback will be executed one final time after the last throttled-function call.
 *                              (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset)
 * @param  {Function} callback - A function to be executed after delay milliseconds. The `this` context and all arguments are passed
 *                             through, as-is, to `callback` when the throttled-function is executed.
 * @param  {Boolean} debounceMode - If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms.
 *                                If `debounceMode` is false (at end), schedule `callback` to execute after `delay` ms.
 *
 * @return {Function} A new, throttled, function.
 */

exports.default = addEventListeners;

},{"throttle-debounce/throttle":2}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Helper function to determine the handler type
 * @module  checkLimit
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * check length vs limit and returns  either `0`, `1` or `2`
 * where:
 * - 0 = none
 * - 1 = default
 * - 2 = throttled
 * @type {Function}
 * @const
 * @private
 * @param  {Number} length - length of array
 * @param  {Number} limit - limit of items in array
 * @return {Number} returns a number (one of `[0,1,2]`)
 */
var checkLimit = function checkLimit(length, limit) {
  return !length ? 0 : length > limit ? 2 : 1;
};

exports.default = checkLimit;

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Array of events to reuse
 * @module  EVENTS
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * all events stored for quick access
 * @type {Array}
 * @const
 * @private
 */
var EVENTS = ['scroll', 'resize', 'mousewheel', 'mousemove', 'mouseup', 'touchmove', 'touchend'];

exports.default = EVENTS;

},{}],6:[function(require,module,exports){
'use strict';

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}(); /**
      * OneListener attempts to gather a collection of handlers to be executed during an event.
      * Usually developers register the handlers directly on the `window` which can cause memory leaks due to unremoved `eventListeners`.
      *
      * When a lot of handlers are active the performance of the app or page might be reduced.
      * One Listener throttles the events globally therefore the handlrers will automatically be throttled.
      * This is based on a handler:limit ratio which can be defined when creating an instance or changed during runtime.
      *
      * @module  one-listener
      *
      * @author  Gregor Adams  <greg@pixelass.com>
      * @author  Jan Nicklas
      * @example
      * import OneListener from 'one-listener'
      * const one = new OneListener({
      *   limit: 6,
      *   throttle: 200
      * })
      * const {requestEventListener, cancelEventListener} = one
      */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 * @author niksy
 * @private
 * @type {Function}
 * @const
 * @param  {Number} delay - A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250
 *                        (or even higher) are most useful.
 * @param  {Boolean} noTrailing - Optional, defaults to false. If noTrailing is true, callback will only execute every
 *                              `delay` milliseconds while the throttled-function is being called. If noTrailing is false
 *                              or unspecified, callback will be executed one final time after the last throttled-function call.
 *                              (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset)
 * @param  {Function} callback - A function to be executed after delay milliseconds. The `this` context and all arguments are passed
 *                             through, as-is, to `callback` when the throttled-function is executed.
 * @param  {Boolean} debounceMode - If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms.
 *                                If `debounceMode` is false (at end), schedule `callback` to execute after `delay` ms.
 *
 * @return {Function} A new, throttled, function.
 */

var _throttle = require('throttle-debounce/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

var _checkLimit2 = require('./check-limit');

var _checkLimit3 = _interopRequireDefault(_checkLimit2);

var _removeEventListeners = require('./remove-event-listeners');

var _removeEventListeners2 = _interopRequireDefault(_removeEventListeners);

var _addEventListeners = require('./add-event-listeners');

var _addEventListeners2 = _interopRequireDefault(_addEventListeners);

var _events = require('./events');

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

/**
 * Creates an instance of OneListener. An instance will handle each event seperately but uses a global throttle and limit.
 * If `scroll` is throttled all requested handlers will return "throttled", while all `resize` handlers rely on their own limit:length ratio.
 * You can create multiple instances to allow adding permanently "throttled" or "unthrottled" `eventListeners` aside of your normal instance.
 *
 * Let's say you have a parallax page which relies on rapid tracking: This would be requested on an unthrottled instance.
 * In case you want to reposition an element on resize it might be wise to request on the throttled instance
 * All handlers are wrapped in a `requestAnimationFrame` to use the browsers default throttling mechanism.
 * @type {Class}
 * @param {Object} options - custom options
 * @param {Number} options.limit - if handlers.length exceeds this value the global eventListener will be throttled
 * @param {Number} options.throttle - if throttled handlers will only be called every `N ms`
 * @return {module:one-listener~OneListener} returns OneListener instance
 *
 * @example
 * // default
 * const one = new OneListener()
 * // custom
 * const one = new OneListener({
 *   limit: 3,
 *   throttle: 500
 * })
 */

var OneListener = function () {
  /**
   * builds and returns an instance of OneListener
   */

  function OneListener(options) {
    _classCallCheck(this, OneListener);

    /**
     * add some options to customize the behavior
     * @type {Object}
     * @private
     * @alias options
     * @memberof module:one-listener~OneListener
     * @property {Number} limit - if handlers.length exceeds this value the global eventListener will be throttled
     * @property {Number} throttle - if throttled handlers will only be called every `N ms`
     */
    this.options = Object.assign({
      limit: 10,
      throttle: 100
    }, options);

    this.handleScroll = this.handleScroll.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.handleMousewheel = this.handleMousewheel.bind(this);
    this.handleMousemove = this.handleMousemove.bind(this);
    this.handleMouseup = this.handleMouseup.bind(this);
    this.handleTouchmove = this.handleTouchmove.bind(this);
    this.handleTouchend = this.handleTouchend.bind(this);
    this.requestEventListener = this.requestEventListener.bind(this);
    this.cancelEventListener = this.cancelEventListener.bind(this);

    this.init();
  }

  /**
   * initialize OneListener.
   * This method is only called to set up to configure
   * the instance.
   * @private
   */

  _createClass(OneListener, [{
    key: 'init',
    value: function init() {
      var _this = this;

      /**
       * global collection of listeners
       * This object is the internal store for the event listeners
       * @type {Object}
       * @private
       * @memberof module:one-listener~OneListener
       * @property {Array} scroll - collection of scroll handlers
       * @property {Array} resize - collection of resize handlers
       * @property {Array} mousewheel - collection of mousewheel handlers
       * @property {Array} mousemove - collection of mousemove handlers
       * @property {Array} mouseup - collection of mouseup handlers
       * @property {Array} touchmove - collection of touchmove handlers
       * @property {Array} touchend - collection of touchend handlers
       */
      this.eventListeners = {
        scroll: [],
        resize: [],
        mousewheel: [],
        mousemove: [],
        mouseup: [],
        touchmove: [],
        touchend: []
      };

      /*
       * the state object will help to check if we need to update the eventListener
       * due to switching from or to a throttled listener.
       * where:
       * - 0 = none
       * - 1 = default
       * - 2 = throttled
       * @private
       * @memberof module:one-listener~OneListener
       * @property {Number} scroll - one of `[0,1,2]`
       * @property {Number} resize - one of `[0,1,2]`
       * @property {Number} mousewheel - one of `[0,1,2]`
       * @property {Number} mousemove - one of `[0,1,2]`
       * @property {Number} mouseup - one of `[0,1,2]`
       * @property {Number} touchmove - one of `[0,1,2]`
       * @property {Number} touchend - one of `[0,1,2]`
       */
      this.state = {
        scroll: 0,
        resize: 0,
        mousewheel: 0,
        mousemove: 0,
        mouseup: 0,
        touchmove: 0,
        touchend: 0
      };

      /**
       * add handlers for all events
       * provides a throttled and default handler
       * @private
       * @memberof module:one-listener~OneListener
       * @property {Object} scroll - scroll handlers
       * @property {Function} scroll.default - default scroll handler
       * @property {Function} scroll.throttled - throttled scroll handler
       * @property {Object} resize - resize handlers
       * @property {Function} resize.default - default resize handler
       * @property {Function} resize.throttled - throttled resize handler
       * @property {Object} mousewheel - mousewheel handlers
       * @property {Function} mousewheel.default - default mousewheel handler
       * @property {Function} mousewheel.throttled - throttled mousewheel handler
       * @property {Object} mousemove - mousemove handlers
       * @property {Function} mousemove.default - default mousemove handler
       * @property {Function} mousemove.throttled - throttled mousemove handler
       * @property {Object} mouseup - mouseup handlers
       * @property {Function} mouseup.default - default mouseup handler
       * @property {Function} mouseup.throttled - default mouseup handler (always unthrottled)
       * @property {Object} touchmove - touchmove handlers
       * @property {Function} touchmove.default - default touchmove handler
       * @property {Function} touchmove.throttled - throttled touchmove handler
       * @property {Object} touchup - touchup handlers
       * @property {Function} touchend.default - default touchend handler
       * @property {Function} touchend.throttled - default touchend handler (always unthrottled)
       */
      this.handlers = {
        scroll: {
          default: this.handleScroll,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleScroll(e);
          })
        },
        resize: {
          default: this.handleResize,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleResize(e);
          })
        },
        mousewheel: {
          default: this.handleMousewheel,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleMousewheel(e);
          })
        },
        mousemove: {
          default: this.handleMousemove,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleMousemove(e);
          })
        },
        mouseup: {
          default: this.handleMouseup,
          // never throttle mouseup
          throttled: this.handleMouseup
        },
        touchmove: {
          default: this.handleTouchmove,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleTouchmove(e);
          })
        },
        touchend: {
          default: this.handleTouchend,
          // never throttle touchup
          throttled: this.handleTouchend
        }
      };

      this.checkLimitAll(true);
    }

    /**
     * check limit of handlers vs settings and update listeners to throttle when reached
     * @private
     * @param  {String} event - handlers to check
     * @param  {Boolean} force - force update
     */

  }, {
    key: 'checkLimit',
    value: function checkLimit(event, force) {
      var cache = this.state[event];
      // set throttled or default handlers depending on the limit:length ratio
      // if no handlers exist don't add an eventListener
      var update = (0, _checkLimit3.default)(this.eventListeners[event].length, this.options.limit);
      // only apply if the state has changed
      if (update !== cache || force) {
        this.state[event] = update;
        (0, _removeEventListeners2.default)(event, this.handlers[event]);
        switch (update) {
          case 2:
            this.handlers[event] = (0, _addEventListeners2.default)(event, this.handlers[event], this.options.throttle);
            break;
          case 1:
            this.handlers[event] = (0, _addEventListeners2.default)(event, this.handlers[event]);
            break;
          case 0:
            break;
          default:
            throw new Error('state should be on of [0,1,2] but was: ' + this.state[event]);
        }
      }
    }

    /** shortcut to check all handlers that are currently requested
     * @private
     * @param {Boolean} force - forces an update (useful when initializing or similar)
     */

  }, {
    key: 'checkLimitAll',
    value: function checkLimitAll(force) {
      var _this2 = this;

      _events2.default.forEach(function (event) {
        return _this2.checkLimit(event, force);
      });
    }

    /**
     * request an eventListener
     * stores the handlesr in the internal storage and returnst a cancel function
     * @public
     * @param  {String} event - name of the event to request
     * @param  {Function} handler - default eventListener handler
     * @return {Function} returns a function which will cancel the eventListener
     *
     * @example
     * const one = new OneListener()
     * const {requestEventListener} = one
     * // simple listener
     * requestEventListener('scroll', handleScroll)
     * // with cancel
     * const cancelScroll = requestEventListener('scroll', handleScroll)
     * // call cancelEventlistener via cancelScroll()
     * cancelScroll() // listener canceled
     */

  }, {
    key: 'requestEventListener',
    value: function requestEventListener(event, handler) {
      var _this3 = this;

      if (!this.eventListeners.hasOwnProperty(event)) {
        throw new Error('Unkown event ' + event);
      }
      this.eventListeners[event].push(handler);
      this.checkLimit(event);
      return function () {
        return _this3.cancelEventListener(event, handler);
      };
    }

    /**
     * cancels an eventListener
     * looks for the handler and removes it from the list
     * @public
     * @param  {String} event - name of the event to cancel
     * @param  {Function} handler - handler to be removed
     *
     * @example
     * const one = new OneListener()
     * const {cancelEventListener} = one
     * cancelEventListener('scroll', handleScroll)
     */

  }, {
    key: 'cancelEventListener',
    value: function cancelEventListener(event, handler) {
      if (!this.eventListeners.hasOwnProperty(event)) {
        throw new Error('Unkown event ' + event);
      }
      var index = this.eventListeners[event].indexOf(handler);
      // Skip if the handler doesn't exist
      if (index === -1) {
        return;
      }
      // update handlers and rebuild listeners
      this.eventListeners[event].splice(index, 1);
      this.checkLimit(event);
    }

    /**
     * named scroll handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - scroll event
     */

  }, {
    key: 'handleScroll',
    value: function handleScroll(e) {
      this.eventListeners.scroll.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named resize handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - resize event
     */

  }, {
    key: 'handleResize',
    value: function handleResize(e) {
      this.eventListeners.resize.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named mousewheel handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - mousewheel event
     */

  }, {
    key: 'handleMousewheel',
    value: function handleMousewheel(e) {
      this.eventListeners.mousewheel.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named mousemove handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - mousemove event
     */

  }, {
    key: 'handleMousemove',
    value: function handleMousemove(e) {
      this.eventListeners.mousemove.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named mouseup handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers
     * @private
     * @param  {Event} e - mouseup event
     */

  }, {
    key: 'handleMouseup',
    value: function handleMouseup(e) {
      this.eventListeners.mouseup.forEach(function (handler) {
        return handler(e);
      });
    }

    /**
     * named touchmove handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - touchmove event
     */

  }, {
    key: 'handleTouchmove',
    value: function handleTouchmove(e) {
      this.eventListeners.touchmove.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named touchend handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers
     * @private
     * @param  {Event} e - touchend event
     */

  }, {
    key: 'handleTouchend',
    value: function handleTouchend(e) {
      this.eventListeners.touchend.forEach(function (handler) {
        return handler(e);
      });
    }

    /**
     * return all eventListeners
     * @type {Getter}
     * @alias get debug
     * @name get debug
     * @memberof module:one-listener~OneListener
     * @return {Object} the internal store
     *
     * @example
     * const one = new OneListener()
     * console.log(one.debug)
     */

  }, {
    key: 'debug',
    get: function get() {
      return this.eventListeners;
    }

    /**
     * set limit of handlers
     * @type {Setter}
     * @alias set limit
     * @name set limit
     * @memberof module:one-listener~OneListener
     * @param  {Number} [value] modifies the limit
     *
     * @example
     * const one = new OneListener()
     * one.limit = 4
     */

  }, {
    key: 'limit',
    set: function set(value) {
      if (typeof value === 'number') {
        this.options.limit = value;
        this.checkLimitAll(true);
      } else {
        throw new Error('value should be of type "number" instead got ' + (typeof value === 'undefined' ? 'undefined' : _typeof(value)));
      }
    }

    /**
     * get limit of handlers
     * @type {Getter}
     * @alias get limit
     * @name get limit
     * @memberof module:one-listener~OneListener
     * @return {Number} returns the current limit
     *
     * @example
     * const one = new OneListener()
     * let limit = one.limit
     */

    , get: function get() {
      return this.options.limit;
    }

    /**
     * set throttle of handlers
     * @type {Setter}
     * @alias set throttle
     * @name set throttle
     * @memberof module:one-listener~OneListener
     * @param  {Number} [value] modifies the throttle
     *
     * @example
     * const one = new OneListener()
     * one.throttle = 500
     */

  }, {
    key: 'throttle',
    set: function set(value) {
      if (typeof value === 'number') {
        this.options.throttle = value;
        this.checkLimitAll(true);
      } else {
        throw new Error('value should be of type "number" instead got ' + (typeof value === 'undefined' ? 'undefined' : _typeof(value)));
      }
    }

    /**
    * get throttle of handlers
    * @type {Getter}
    * @alias get throttle
    * @name get throttle
    * @memberof module:one-listener~OneListener
    * @return {Number} returns the current throttle
    *
    * @example
    * const one = new OneListener()
    * let throttle = one.throttle
    */

    , get: function get() {
      return this.options.throttle;
    }
  }]);

  return OneListener;
}();

exports.default = OneListener;

},{"./add-event-listeners":3,"./check-limit":4,"./events":5,"./remove-event-listeners":7,"throttle-debounce/throttle":2}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Helper function to remove eventListeners from `window`
 * @module  removeEventListeners
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * remove all listeners that are potentially on `window`
 * @type {Function}
 * @const
 * @private
 * @param  {String} event   event to forget
 * @param  {Object} handler - the handler object containing the throttled and unthrottled handlers
 * @param  {Function} handler.default - default handler
 * @param  {Function} handler.throttle - throttled handler
 */
var removeEventListeners = function removeEventListeners(event, handler) {
  window.removeEventListener(event, handler.throttle);
  window.removeEventListener(event, handler.default);
};

exports.default = removeEventListeners;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJleGFtcGxlcy9leGFtcGxlcy5zcmMuanMiLCJub2RlX21vZHVsZXMvdGhyb3R0bGUtZGVib3VuY2UvdGhyb3R0bGUuanMiLCJzcmMvYWRkLWV2ZW50LWxpc3RlbmVycy5qcyIsInNyYy9jaGVjay1saW1pdC5qcyIsInNyYy9ldmVudHMuanMiLCJzcmMvaW5kZXguanMiLCJzcmMvcmVtb3ZlLWV2ZW50LWxpc3RlbmVycy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUE7Ozs7Ozs7O0FBRUEsSUFBTTtTQUFzQixBQUNuQixBQUNQO1lBRkYsQUFBWSxBQUFnQixBQUVoQjtBQUZnQixBQUMxQixDQURVO0FBSVosSUFBTTtTQUFOLEFBQWMsQUFBZ0IsQUFDckI7QUFEcUIsQUFDNUIsQ0FEWTtBQUdkLElBQU07U0FBeUIsQUFDdEIsQUFDUDtZQUZGLEFBQWUsQUFBZ0IsQUFFbkI7QUFGbUIsQUFDN0IsQ0FEYTtJLEFBSVIsdUIsQUFBNkMsSSxBQUE3QztJLEFBQXNCLHNCLEFBQXVCLEksQUFBdkI7O0FBRTdCLFFBQUEsQUFBUSxJQUFSLEFBQVksS0FBWixBQUFpQjs7O0FBR2pCLElBQU0sd0NBQW1CLEFBQXFCLGFBQWEsVUFBQSxBQUFDLEdBQU0sQUFDaEU7VUFBQSxBQUFRO09BQ0gsRUFETyxBQUNMLEFBQ0w7T0FBRyxFQUZMLEFBQVksQUFFTCxBQUVSO0FBSmEsQUFDVjtBQUZKLEFBQXlCLENBQUE7O0FBT3pCLElBQUksT0FBSixBQUFXOzs7QUFHWCxJQUFNLGNBQWMsU0FBZCxBQUFjLFlBQUEsQUFBQyxHQUFNLEFBQ3pCO1VBQUEsQUFBUSxJQUFJLE9BQVosQUFBbUIsQUFDbkI7TUFBSSxPQUFBLEFBQU8sVUFBWCxBQUFxQixLQUFLLEFBQ3hCO1FBQUEsQUFBSSxNQUFNLEFBQ1I7Y0FBQSxBQUFRLElBQVIsQUFBWSxBQUNaO0FBQ0E7YUFBQSxBQUFPLEFBQ1I7QUFDRjtBQUNGO0FBVEQ7O0FBV0EsSUFBTSxPQUFPLFNBQVAsQUFBTyxPQUFNLEFBQ2xCLENBREQ7QUFFQSxxQkFBQSxBQUFxQixVQUFyQixBQUErQjs7QUFFL0IsS0FBSyxJQUFJLElBQVQsQUFBYSxHQUFHLElBQWhCLEFBQW9CLEdBQXBCLEFBQXVCLEtBQUssQUFDMUI7dUJBQUEsQUFBcUIsVUFBckIsQUFBK0IsQUFDaEM7OztBQUVELEtBQUssSUFBSSxLQUFULEFBQWEsR0FBRyxLQUFoQixBQUFvQixHQUFwQixBQUF1QixNQUFLLEFBQzFCO3VCQUFBLEFBQXFCLFVBQXJCLEFBQStCLEFBQ2hDOzs7QUFFRCxxQkFBQSxBQUFxQixVQUFVLFVBQUEsQUFBQyxHQUFNLEFBQ3BDO1VBQUEsQUFBUSxJQUFSLEFBQVksQUFDYjtBQUZEOztBQUtBLFdBQVcsWUFBTSxBQUNmO01BQUEsQUFBSSxXQUFKLEFBQWUsQUFDZjtNQUFBLEFBQUksUUFBSixBQUFZLEFBQ1o7VUFBQSxBQUFRLElBQVIsQUFBWSxBQUNaO2FBQVcsWUFBTSxBQUNmO3dCQUFBLEFBQW9CLFVBQXBCLEFBQThCLEFBQzlCO1FBQUEsQUFBSSxXQUFKLEFBQWUsQUFDZjtRQUFBLEFBQUksUUFBSixBQUFZLEFBQ1o7WUFBQSxBQUFRLElBQVIsQUFBWSxBQUNaO1lBQUEsQUFBUSxJQUFSLEFBQVksQUFDWjtZQUFBLEFBQVEsbUNBQWlDLElBQXpDLEFBQTZDLFdBQzlDO0FBUEQsS0FBQSxBQU9HLEFBQ0o7QUFaRCxHQUFBLEFBWUc7O0FBRUgsUUFBQSxBQUFRLElBQVIsQUFBWSxXQUFXLElBQXZCLEFBQTJCO0FBQzNCLFFBQUEsQUFBUSxJQUFSLEFBQVksY0FBYyxJQUExQixBQUE4QjtBQUM5QixRQUFBLEFBQVEsSUFBUixBQUFZLGdCQUFnQixJQUE1QixBQUFnQzs7QUFFaEMsTUFBQSxBQUFNLHFCQUFOLEFBQTJCLFVBQVUsVUFBQSxBQUFDLEdBQU0sQUFDMUM7VUFBQSxBQUFRLElBQVIsQUFBWSxBQUNiO0FBRkQ7QUFHQSxPQUFBLEFBQU8scUJBQVAsQUFBNEIsVUFBVSxVQUFBLEFBQUMsR0FBTSxBQUMzQztVQUFBLEFBQVEsNkJBQTJCLE9BQW5DLEFBQTBDLFdBQzNDO0FBRkQ7OztBQzdFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2hFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFjQSxJQUFNLG9CQUFvQixTQUFwQixBQUFvQixrQkFBQSxBQUFDLE9BQUQsQUFBUSxTQUF1QjtNQUFkLEFBQWMsOERBQU4sQUFBTSxjQUN2RDs7TUFBSSxRQUFKLEFBQVksR0FBRyxBQUNiO1lBQUEsQUFBUSxtQ0FBVyxBQUFTLE9BQU8sVUFBQSxBQUFDLEdBQUQ7YUFBTyxRQUFBLEFBQVEsUUFBZixBQUFPLEFBQWdCO0FBQTFELEFBQW1CLEFBQ25CLEtBRG1CO1dBQ25CLEFBQU8saUJBQVAsQUFBd0IsT0FBTyxRQUEvQixBQUF1QyxBQUN4QztBQUhELFNBR08sQUFDTDtXQUFBLEFBQU8saUJBQVAsQUFBd0IsT0FBTyxRQUEvQixBQUF1QyxBQUN4QztBQUNEO1NBQUEsQUFBTyxBQUNSO0EsQUFSRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztrQixBQVVlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0JmLElBQU0sYUFBYSxTQUFiLEFBQWEsV0FBQSxBQUFDLFFBQUQsQUFBUyxPQUFUO1NBQW1CLENBQUEsQUFBQyxTQUFELEFBQVUsSUFBSSxTQUFBLEFBQVMsUUFBVCxBQUFpQixJQUFsRCxBQUFzRDtBQUF6RTs7a0IsQUFFZTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVGYsSUFBTSxTQUFTLENBQUEsQUFBQyxVQUFELEFBQVcsVUFBWCxBQUFxQixjQUFyQixBQUFtQyxhQUFuQyxBQUFnRCxXQUFoRCxBQUEyRCxhQUExRSxBQUFlLEFBQXdFOztrQixBQUV4RTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzBCZjs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJLEFBeUJNLDBCQUlKOzs7Ozt1QkFBQSxBQUFhLFNBQVM7MEJBVXBCOzs7Ozs7Ozs7OztTQUFBLEFBQUssaUJBQVUsQUFBTzthQUFPLEFBQ3BCLEFBQ1A7Z0JBRmEsQUFBYyxBQUVqQjtBQUZpQixBQUMzQixLQURhLEVBQWYsQUFBZSxBQUdaLEFBRUg7O1NBQUEsQUFBSyxlQUFlLEtBQUEsQUFBSyxhQUFMLEFBQWtCLEtBQXRDLEFBQW9CLEFBQXVCLEFBQzNDO1NBQUEsQUFBSyxlQUFlLEtBQUEsQUFBSyxhQUFMLEFBQWtCLEtBQXRDLEFBQW9CLEFBQXVCLEFBQzNDO1NBQUEsQUFBSyxtQkFBbUIsS0FBQSxBQUFLLGlCQUFMLEFBQXNCLEtBQTlDLEFBQXdCLEFBQTJCLEFBQ25EO1NBQUEsQUFBSyxrQkFBa0IsS0FBQSxBQUFLLGdCQUFMLEFBQXFCLEtBQTVDLEFBQXVCLEFBQTBCLEFBQ2pEO1NBQUEsQUFBSyxnQkFBZ0IsS0FBQSxBQUFLLGNBQUwsQUFBbUIsS0FBeEMsQUFBcUIsQUFBd0IsQUFDN0M7U0FBQSxBQUFLLGtCQUFrQixLQUFBLEFBQUssZ0JBQUwsQUFBcUIsS0FBNUMsQUFBdUIsQUFBMEIsQUFDakQ7U0FBQSxBQUFLLGlCQUFpQixLQUFBLEFBQUssZUFBTCxBQUFvQixLQUExQyxBQUFzQixBQUF5QixBQUMvQztTQUFBLEFBQUssdUJBQXVCLEtBQUEsQUFBSyxxQkFBTCxBQUEwQixLQUF0RCxBQUE0QixBQUErQixBQUMzRDtTQUFBLEFBQUssc0JBQXNCLEtBQUEsQUFBSyxvQkFBTCxBQUF5QixLQUFwRCxBQUEyQixBQUE4QixBQUV6RDs7U0FBQSxBQUFLLEFBQ047Ozs7Ozs7Ozs7OzsyQkFRTztrQkFlTjs7Ozs7Ozs7Ozs7Ozs7OztXQUFBLEFBQUs7Z0JBQWlCLEFBQ1osQUFDUjtnQkFGb0IsQUFFWixBQUNSO29CQUhvQixBQUdSLEFBQ1o7bUJBSm9CLEFBSVQsQUFDWDtpQkFMb0IsQUFLWCxBQUNUO21CQU5vQixBQU1ULEFBQ1g7a0JBUEYsQUFBc0IsQUFPVixBQW9CWjtBQTNCc0IsQUFDcEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7V0EwQkYsQUFBSztnQkFBUSxBQUNILEFBQ1I7Z0JBRlcsQUFFSCxBQUNSO29CQUhXLEFBR0MsQUFDWjttQkFKVyxBQUlBLEFBQ1g7aUJBTFcsQUFLRixBQUNUO21CQU5XLEFBTUEsQUFDWDtrQkFQRixBQUFhLEFBT0QsQUE4Qlo7QUFyQ2EsQUFDWDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7V0FvQ0YsQUFBSzs7bUJBRVEsS0FESCxBQUNRLEFBQ2Q7NkNBQW9CLEtBQUEsQUFBSyxRQUFkLEFBQXNCLFVBQVUsVUFBQSxBQUFDLEdBQUQ7bUJBQU8sTUFBQSxBQUFLLGFBQVosQUFBTyxBQUFrQjtBQUh4RCxBQUNOLEFBRUssQUFFYixXQUZhO0FBRkwsQUFDTjs7bUJBSVMsS0FESCxBQUNRLEFBQ2Q7NkNBQW9CLEtBQUEsQUFBSyxRQUFkLEFBQXNCLFVBQVUsVUFBQSxBQUFDLEdBQUQ7bUJBQU8sTUFBQSxBQUFLLGFBQVosQUFBTyxBQUFrQjtBQVB4RCxBQUtOLEFBRUssQUFFYixXQUZhO0FBRkwsQUFDTjs7bUJBSVMsS0FEQyxBQUNJLEFBQ2Q7NkNBQW9CLEtBQUEsQUFBSyxRQUFkLEFBQXNCLFVBQVUsVUFBQSxBQUFDLEdBQUQ7bUJBQU8sTUFBQSxBQUFLLGlCQUFaLEFBQU8sQUFBc0I7QUFYNUQsQUFTRixBQUVDLEFBRWIsV0FGYTtBQUZELEFBQ1Y7O21CQUlTLEtBREEsQUFDSyxBQUNkOzZDQUFvQixLQUFBLEFBQUssUUFBZCxBQUFzQixVQUFVLFVBQUEsQUFBQyxHQUFEO21CQUFPLE1BQUEsQUFBSyxnQkFBWixBQUFPLEFBQXFCO0FBZjNELEFBYUgsQUFFRSxBQUViLFdBRmE7QUFGRixBQUNUOzttQkFJUyxLQURGLEFBQ08sQUFFZDs7cUJBQVcsS0FwQkMsQUFpQkwsQUFHUyxBQUVsQjtBQUxTLEFBQ1A7O21CQUtTLEtBREEsQUFDSyxBQUNkOzZDQUFvQixLQUFBLEFBQUssUUFBZCxBQUFzQixVQUFVLFVBQUEsQUFBQyxHQUFEO21CQUFPLE1BQUEsQUFBSyxnQkFBWixBQUFPLEFBQXFCO0FBeEIzRCxBQXNCSCxBQUVFLEFBRWIsV0FGYTtBQUZGLEFBQ1Q7O21CQUlTLEtBREQsQUFDTSxBQUVkOztxQkFBVyxLQTdCZixBQUFnQixBQTBCSixBQUdRLEFBSXBCO0FBUFksQUFDUjtBQTNCWSxBQUNkOztXQWdDRixBQUFLLGNBQUwsQUFBbUIsQUFDcEI7Ozs7Ozs7Ozs7OzsrQixBQVFXLE8sQUFBTyxPQUFPLEFBQ3hCO1VBQU0sUUFBUSxLQUFBLEFBQUssTUFBbkIsQUFBYyxBQUFXLEFBR3pCOzs7VUFBTSxTQUFTLDBCQUFXLEtBQUEsQUFBSyxlQUFMLEFBQW9CLE9BQS9CLEFBQXNDLFFBQVEsS0FBQSxBQUFLLFFBQWxFLEFBQWUsQUFBMkQsQUFFMUU7O1VBQUksV0FBQSxBQUFXLFNBQWYsQUFBd0IsT0FBTyxBQUM3QjthQUFBLEFBQUssTUFBTCxBQUFXLFNBQVgsQUFBb0IsQUFDcEI7NENBQUEsQUFBcUIsT0FBTyxLQUFBLEFBQUssU0FBakMsQUFBNEIsQUFBYyxBQUMxQztnQkFBQSxBQUFRLEFBQ047ZUFBQSxBQUFLLEFBQ0g7aUJBQUEsQUFBSyxTQUFMLEFBQWMsU0FBUyxpQ0FBQSxBQUFrQixPQUFPLEtBQUEsQUFBSyxTQUE5QixBQUF5QixBQUFjLFFBQVEsS0FBQSxBQUFLLFFBQTNFLEFBQXVCLEFBQTRELEFBQ25GO0FBQ0Y7ZUFBQSxBQUFLLEFBQ0g7aUJBQUEsQUFBSyxTQUFMLEFBQWMsU0FBUyxpQ0FBQSxBQUFrQixPQUFPLEtBQUEsQUFBSyxTQUFyRCxBQUF1QixBQUF5QixBQUFjLEFBQzlEO0FBQ0Y7ZUFBQSxBQUFLLEFBQ0g7QUFDRjtBQUNFO2tCQUFNLElBQUEsQUFBSSxNQUFNLDRDQUE0QyxLQUFBLEFBQUssTUFWckUsQUFVSSxBQUFNLEFBQXNELEFBQVcsQUFFNUU7O0FBQ0Y7Ozs7Ozs7Ozs7a0MsQUFNYyxPQUFPO21CQUNwQjs7dUJBQUEsQUFBTyxRQUFRLFVBQUEsQUFBQyxPQUFEO2VBQVcsT0FBQSxBQUFLLFdBQUwsQUFBZ0IsT0FBM0IsQUFBVyxBQUF1QjtBQUFqRCxBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7eUMsQUFvQnFCLE8sQUFBTyxTQUFTO21CQUNwQzs7VUFBSSxDQUFDLEtBQUEsQUFBSyxlQUFMLEFBQW9CLGVBQXpCLEFBQUssQUFBbUMsUUFBUSxBQUM5QztjQUFNLElBQUEsQUFBSSxNQUFNLGtCQUFoQixBQUFNLEFBQTRCLEFBQ25DO0FBQ0Q7V0FBQSxBQUFLLGVBQUwsQUFBb0IsT0FBcEIsQUFBMkIsS0FBM0IsQUFBZ0MsQUFDaEM7V0FBQSxBQUFLLFdBQUwsQUFBZ0IsQUFDaEI7YUFBTyxZQUFBO2VBQU0sT0FBQSxBQUFLLG9CQUFMLEFBQXlCLE9BQS9CLEFBQU0sQUFBZ0M7QUFBN0MsQUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O3dDLEFBY29CLE8sQUFBTyxTQUFTLEFBQ25DO1VBQUksQ0FBQyxLQUFBLEFBQUssZUFBTCxBQUFvQixlQUF6QixBQUFLLEFBQW1DLFFBQVEsQUFDOUM7Y0FBTSxJQUFBLEFBQUksTUFBTSxrQkFBaEIsQUFBTSxBQUE0QixBQUNuQztBQUNEO1VBQU0sUUFBUSxLQUFBLEFBQUssZUFBTCxBQUFvQixPQUFwQixBQUEyQixRQUF6QyxBQUFjLEFBQW1DLEFBRWpEOztVQUFJLFVBQVUsQ0FBZCxBQUFlLEdBQUcsQUFDaEI7QUFDRDtBQUVEOztXQUFBLEFBQUssZUFBTCxBQUFvQixPQUFwQixBQUEyQixPQUEzQixBQUFrQyxPQUFsQyxBQUF5QyxBQUN6QztXQUFBLEFBQUssV0FBTCxBQUFnQixBQUNqQjs7Ozs7Ozs7Ozs7O2lDLEFBUWEsR0FBRyxBQUNmO1dBQUEsQUFBSyxlQUFMLEFBQW9CLE9BQXBCLEFBQTJCLFFBQVEsVUFBQSxBQUFDLFNBQUQ7c0JBQWEsQUFBTyxzQkFBc0IsWUFBQTtpQkFBTSxRQUFOLEFBQU0sQUFBUTtBQUF4RCxBQUFhLFNBQUE7QUFBaEQsQUFDRDs7Ozs7Ozs7Ozs7O2lDLEFBUWEsR0FBRyxBQUNmO1dBQUEsQUFBSyxlQUFMLEFBQW9CLE9BQXBCLEFBQTJCLFFBQVEsVUFBQSxBQUFDLFNBQUQ7c0JBQWEsQUFBTyxzQkFBc0IsWUFBQTtpQkFBTSxRQUFOLEFBQU0sQUFBUTtBQUF4RCxBQUFhLFNBQUE7QUFBaEQsQUFDRDs7Ozs7Ozs7Ozs7O3FDLEFBUWlCLEdBQUcsQUFDbkI7V0FBQSxBQUFLLGVBQUwsQUFBb0IsV0FBcEIsQUFBK0IsUUFBUSxVQUFBLEFBQUMsU0FBRDtzQkFBYSxBQUFPLHNCQUFzQixZQUFBO2lCQUFNLFFBQU4sQUFBTSxBQUFRO0FBQXhELEFBQWEsU0FBQTtBQUFwRCxBQUNEOzs7Ozs7Ozs7Ozs7b0MsQUFRZ0IsR0FBRyxBQUNsQjtXQUFBLEFBQUssZUFBTCxBQUFvQixVQUFwQixBQUE4QixRQUFRLFVBQUEsQUFBQyxTQUFEO3NCQUFhLEFBQU8sc0JBQXNCLFlBQUE7aUJBQU0sUUFBTixBQUFNLEFBQVE7QUFBeEQsQUFBYSxTQUFBO0FBQW5ELEFBQ0Q7Ozs7Ozs7Ozs7OztrQyxBQVFjLEdBQUcsQUFDaEI7V0FBQSxBQUFLLGVBQUwsQUFBb0IsUUFBcEIsQUFBNEIsUUFBUSxVQUFBLEFBQUMsU0FBRDtlQUFhLFFBQWIsQUFBYSxBQUFRO0FBQXpELEFBQ0Q7Ozs7Ozs7Ozs7OztvQyxBQVFnQixHQUFHLEFBQ2xCO1dBQUEsQUFBSyxlQUFMLEFBQW9CLFVBQXBCLEFBQThCLFFBQVEsVUFBQSxBQUFDLFNBQUQ7c0JBQWEsQUFBTyxzQkFBc0IsWUFBQTtpQkFBTSxRQUFOLEFBQU0sQUFBUTtBQUF4RCxBQUFhLFNBQUE7QUFBbkQsQUFDRDs7Ozs7Ozs7Ozs7O21DLEFBUWUsR0FBRyxBQUNqQjtXQUFBLEFBQUssZUFBTCxBQUFvQixTQUFwQixBQUE2QixRQUFRLFVBQUEsQUFBQyxTQUFEO2VBQWEsUUFBYixBQUFhLEFBQVE7QUFBMUQsQUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O3dCQWNZLEFBQ1g7YUFBTyxLQUFQLEFBQVksQUFDYjs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NCLEFBY1UsT0FBTyxBQUNoQjtVQUFJLE9BQUEsQUFBTyxVQUFYLEFBQXFCLFVBQVUsQUFDN0I7YUFBQSxBQUFLLFFBQUwsQUFBYSxRQUFiLEFBQXFCLEFBQ3JCO2FBQUEsQUFBSyxjQUFMLEFBQW1CLEFBQ3BCO0FBSEQsYUFHTyxBQUNMO2NBQU0sSUFBQSxBQUFJLE1BQU0sMERBQUEsQUFBeUQsOENBQXpFLEFBQU0sQUFBVSxBQUF5RCxBQUMxRTtBQUNGOzs7Ozs7Ozs7Ozs7Ozs7OzBCQWNZLEFBQ1g7YUFBTyxLQUFBLEFBQUssUUFBWixBQUFvQixBQUNyQjs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NCLEFBY2EsT0FBTyxBQUNuQjtVQUFJLE9BQUEsQUFBTyxVQUFYLEFBQXFCLFVBQVUsQUFDN0I7YUFBQSxBQUFLLFFBQUwsQUFBYSxXQUFiLEFBQXdCLEFBQ3hCO2FBQUEsQUFBSyxjQUFMLEFBQW1CLEFBQ3BCO0FBSEQsYUFHTyxBQUNMO2NBQU0sSUFBQSxBQUFJLE1BQU0sMERBQUEsQUFBeUQsOENBQXpFLEFBQU0sQUFBVSxBQUF5RCxBQUMxRTtBQUNGOzs7Ozs7Ozs7Ozs7Ozs7OzBCQWNlLEFBQ2Q7YUFBTyxLQUFBLEFBQUssUUFBWixBQUFvQixBQUNyQjs7Ozs7OztrQixBQUlZOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNWNmLElBQU0sdUJBQXVCLFNBQXZCLEFBQXVCLHFCQUFBLEFBQUMsT0FBRCxBQUFRLFNBQVksQUFDL0M7U0FBQSxBQUFPLG9CQUFQLEFBQTJCLE9BQU8sUUFBbEMsQUFBMEMsQUFDMUM7U0FBQSxBQUFPLG9CQUFQLEFBQTJCLE9BQU8sUUFBbEMsQUFBMEMsQUFDM0M7QUFIRDs7a0IsQUFLZSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJpbXBvcnQgT25lTGlzdGVuZXIgZnJvbSAnLi4vc3JjLyc7XG5cbmNvbnN0IG9uZSA9IG5ldyBPbmVMaXN0ZW5lcih7XG4gIGxpbWl0OiA2LFxuICB0aHJvdHRsZTogMjAwXG59KTtcbmNvbnN0IG5ldmVyID0gbmV3IE9uZUxpc3RlbmVyKHtcbiAgbGltaXQ6IEluZmluaXR5XG59KTtcbmNvbnN0IGFsd2F5cyA9IG5ldyBPbmVMaXN0ZW5lcih7XG4gIGxpbWl0OiAwLFxuICB0aHJvdHRsZTogMTAwXG59KTtcbmNvbnN0IHtyZXF1ZXN0RXZlbnRMaXN0ZW5lciwgY2FuY2VsRXZlbnRMaXN0ZW5lcn0gPSBvbmU7XG5cbmNvbnNvbGUubG9nKG9uZSwgY2FuY2VsRXZlbnRMaXN0ZW5lcik7XG5cbi8vIHJlcXVlc3QgbW91c2Vtb3ZlIFxuY29uc3Qgc3RvcE1vdmVUcmFja2luZyA9IHJlcXVlc3RFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCAoZSkgPT4ge1xuICBjb25zb2xlLmxvZyh7XG4gICAgeDogZS5wYWdlWCxcbiAgICB5OiBlLnBhZ2VZXG4gIH0pO1xufSk7XG5cbmxldCBtb3ZlID0gdHJ1ZTtcbi8vIHJlcXVlc3Qgc2Nyb2xsXG4vLyBhbmQgY2FuY2VsIG1vdXNlbW92ZSBvbiBjb25kaXRpb24gXG5jb25zdCB0cmFja1Njcm9sbCA9IChlKSA9PiB7XG4gIGNvbnNvbGUubG9nKHdpbmRvdy5zY3JvbGxZKTtcbiAgaWYgKHdpbmRvdy5zY3JvbGxZID4gMTAwKSB7XG4gICAgaWYgKG1vdmUpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdtb3VzZW1vdmUgY2FuY2VsZWQnKTtcbiAgICAgIHN0b3BNb3ZlVHJhY2tpbmcoKTtcbiAgICAgIG1vdmUgPSBmYWxzZTtcbiAgICB9XG4gIH1cbn07XG5cbmNvbnN0IG5vb3AgPSAoKSA9PiB7XG59O1xucmVxdWVzdEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRyYWNrU2Nyb2xsKTtcblxuZm9yIChsZXQgaSA9IDA7IGkgPCA4OyBpKyspIHtcbiAgcmVxdWVzdEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIG5vb3ApO1xufVxuXG5mb3IgKGxldCBpID0gMDsgaSA8IDM7IGkrKykge1xuICByZXF1ZXN0RXZlbnRMaXN0ZW5lcigncmVzaXplJywgbm9vcCk7XG59XG5cbnJlcXVlc3RFdmVudExpc3RlbmVyKCdyZXNpemUnLCAoZSkgPT4ge1xuICBjb25zb2xlLmxvZygncmVzaXppbmcgd2luZG93Jyk7XG59KTtcblxuXG5zZXRUaW1lb3V0KCgpID0+IHtcbiAgb25lLnRocm90dGxlID0gMTAwMDtcbiAgb25lLmxpbWl0ID0gMzA7XG4gIGNvbnNvbGUubG9nKCdldmVudHMgdGhyb3R0bGVkJyk7XG4gIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgIGNhbmNlbEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRyYWNrU2Nyb2xsKTtcbiAgICBvbmUudGhyb3R0bGUgPSA1MDAwO1xuICAgIG9uZS5saW1pdCA9IDI7XG4gICAgY29uc29sZS5sb2coJ2V2ZW50cyB0aHJvdHRsZWQgbGVzcycpO1xuICAgIGNvbnNvbGUubG9nKCdzY3JvbGxpbmcgY2FuY2VsZWQnKTtcbiAgICBjb25zb2xlLmxvZyhgdHJ5IHJlc2l6aW5nIHRoZSBicm93c2VyICgke29uZS50aHJvdHRsZX1tcyB0aHJvdHRsZSlgKTtcbiAgfSwgMzAwMCk7XG59LCAzMDAwKTtcblxuY29uc29sZS5sb2coJ2xpbWl0OiAnLCBvbmUubGltaXQpO1xuY29uc29sZS5sb2coJ3Rocm90dGxlOiAnLCBvbmUudGhyb3R0bGUpO1xuY29uc29sZS5sb2coJ0RlYnVnZ2VyOiBcXG4nLCBvbmUuZGVidWcpO1xuXG5uZXZlci5yZXF1ZXN0RXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgKGUpID0+IHtcbiAgY29uc29sZS5sb2coJ25ldmVyIHRocm90dGxlZCcpO1xufSk7XG5hbHdheXMucmVxdWVzdEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIChlKSA9PiB7XG4gIGNvbnNvbGUubG9nKGBhbHdheXMgdGhyb3R0bGVkIGJ5ICR7YWx3YXlzLnRocm90dGxlfW1zYCk7XG59KTtcblxuXG4iLCIvKiBlc2xpbnQtZGlzYWJsZSBuby11bmRlZmluZWQsbm8tcGFyYW0tcmVhc3NpZ24sbm8tc2hhZG93ICovXG5cbi8qKlxuICogVGhyb3R0bGUgZXhlY3V0aW9uIG9mIGEgZnVuY3Rpb24uIEVzcGVjaWFsbHkgdXNlZnVsIGZvciByYXRlIGxpbWl0aW5nXG4gKiBleGVjdXRpb24gb2YgaGFuZGxlcnMgb24gZXZlbnRzIGxpa2UgcmVzaXplIGFuZCBzY3JvbGwuXG4gKlxuICogQHBhcmFtICB7TnVtYmVyfSAgICBkZWxheSAgICAgICAgICBBIHplcm8tb3ItZ3JlYXRlciBkZWxheSBpbiBtaWxsaXNlY29uZHMuIEZvciBldmVudCBjYWxsYmFja3MsIHZhbHVlcyBhcm91bmQgMTAwIG9yIDI1MCAob3IgZXZlbiBoaWdoZXIpIGFyZSBtb3N0IHVzZWZ1bC5cbiAqIEBwYXJhbSAge0Jvb2xlYW59ICAgbm9UcmFpbGluZyAgICAgT3B0aW9uYWwsIGRlZmF1bHRzIHRvIGZhbHNlLiBJZiBub1RyYWlsaW5nIGlzIHRydWUsIGNhbGxiYWNrIHdpbGwgb25seSBleGVjdXRlIGV2ZXJ5IGBkZWxheWAgbWlsbGlzZWNvbmRzIHdoaWxlIHRoZVxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdHRsZWQtZnVuY3Rpb24gaXMgYmVpbmcgY2FsbGVkLiBJZiBub1RyYWlsaW5nIGlzIGZhbHNlIG9yIHVuc3BlY2lmaWVkLCBjYWxsYmFjayB3aWxsIGJlIGV4ZWN1dGVkIG9uZSBmaW5hbCB0aW1lXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFmdGVyIHRoZSBsYXN0IHRocm90dGxlZC1mdW5jdGlvbiBjYWxsLiAoQWZ0ZXIgdGhlIHRocm90dGxlZC1mdW5jdGlvbiBoYXMgbm90IGJlZW4gY2FsbGVkIGZvciBgZGVsYXlgIG1pbGxpc2Vjb25kcyxcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhlIGludGVybmFsIGNvdW50ZXIgaXMgcmVzZXQpXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gIGNhbGxiYWNrICAgICAgIEEgZnVuY3Rpb24gdG8gYmUgZXhlY3V0ZWQgYWZ0ZXIgZGVsYXkgbWlsbGlzZWNvbmRzLiBUaGUgYHRoaXNgIGNvbnRleHQgYW5kIGFsbCBhcmd1bWVudHMgYXJlIHBhc3NlZCB0aHJvdWdoLCBhcy1pcyxcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG8gYGNhbGxiYWNrYCB3aGVuIHRoZSB0aHJvdHRsZWQtZnVuY3Rpb24gaXMgZXhlY3V0ZWQuXG4gKiBAcGFyYW0gIHtCb29sZWFufSAgIGRlYm91bmNlTW9kZSAgIElmIGBkZWJvdW5jZU1vZGVgIGlzIHRydWUgKGF0IGJlZ2luKSwgc2NoZWR1bGUgYGNsZWFyYCB0byBleGVjdXRlIGFmdGVyIGBkZWxheWAgbXMuIElmIGBkZWJvdW5jZU1vZGVgIGlzIGZhbHNlIChhdCBlbmQpLFxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY2hlZHVsZSBgY2FsbGJhY2tgIHRvIGV4ZWN1dGUgYWZ0ZXIgYGRlbGF5YCBtcy5cbiAqXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn0gIEEgbmV3LCB0aHJvdHRsZWQsIGZ1bmN0aW9uLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICggZGVsYXksIG5vVHJhaWxpbmcsIGNhbGxiYWNrLCBkZWJvdW5jZU1vZGUgKSB7XG5cblx0Ly8gQWZ0ZXIgd3JhcHBlciBoYXMgc3RvcHBlZCBiZWluZyBjYWxsZWQsIHRoaXMgdGltZW91dCBlbnN1cmVzIHRoYXRcblx0Ly8gYGNhbGxiYWNrYCBpcyBleGVjdXRlZCBhdCB0aGUgcHJvcGVyIHRpbWVzIGluIGB0aHJvdHRsZWAgYW5kIGBlbmRgXG5cdC8vIGRlYm91bmNlIG1vZGVzLlxuXHR2YXIgdGltZW91dElEO1xuXG5cdC8vIEtlZXAgdHJhY2sgb2YgdGhlIGxhc3QgdGltZSBgY2FsbGJhY2tgIHdhcyBleGVjdXRlZC5cblx0dmFyIGxhc3RFeGVjID0gMDtcblxuXHQvLyBgbm9UcmFpbGluZ2AgZGVmYXVsdHMgdG8gZmFsc3kuXG5cdGlmICggdHlwZW9mIG5vVHJhaWxpbmcgIT09ICdib29sZWFuJyApIHtcblx0XHRkZWJvdW5jZU1vZGUgPSBjYWxsYmFjaztcblx0XHRjYWxsYmFjayA9IG5vVHJhaWxpbmc7XG5cdFx0bm9UcmFpbGluZyA9IHVuZGVmaW5lZDtcblx0fVxuXG5cdC8vIFRoZSBgd3JhcHBlcmAgZnVuY3Rpb24gZW5jYXBzdWxhdGVzIGFsbCBvZiB0aGUgdGhyb3R0bGluZyAvIGRlYm91bmNpbmdcblx0Ly8gZnVuY3Rpb25hbGl0eSBhbmQgd2hlbiBleGVjdXRlZCB3aWxsIGxpbWl0IHRoZSByYXRlIGF0IHdoaWNoIGBjYWxsYmFja2Bcblx0Ly8gaXMgZXhlY3V0ZWQuXG5cdGZ1bmN0aW9uIHdyYXBwZXIgKCkge1xuXG5cdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdHZhciBlbGFwc2VkID0gTnVtYmVyKG5ldyBEYXRlKCkpIC0gbGFzdEV4ZWM7XG5cdFx0dmFyIGFyZ3MgPSBhcmd1bWVudHM7XG5cblx0XHQvLyBFeGVjdXRlIGBjYWxsYmFja2AgYW5kIHVwZGF0ZSB0aGUgYGxhc3RFeGVjYCB0aW1lc3RhbXAuXG5cdFx0ZnVuY3Rpb24gZXhlYyAoKSB7XG5cdFx0XHRsYXN0RXhlYyA9IE51bWJlcihuZXcgRGF0ZSgpKTtcblx0XHRcdGNhbGxiYWNrLmFwcGx5KHNlbGYsIGFyZ3MpO1xuXHRcdH1cblxuXHRcdC8vIElmIGBkZWJvdW5jZU1vZGVgIGlzIHRydWUgKGF0IGJlZ2luKSB0aGlzIGlzIHVzZWQgdG8gY2xlYXIgdGhlIGZsYWdcblx0XHQvLyB0byBhbGxvdyBmdXR1cmUgYGNhbGxiYWNrYCBleGVjdXRpb25zLlxuXHRcdGZ1bmN0aW9uIGNsZWFyICgpIHtcblx0XHRcdHRpbWVvdXRJRCA9IHVuZGVmaW5lZDtcblx0XHR9XG5cblx0XHRpZiAoIGRlYm91bmNlTW9kZSAmJiAhdGltZW91dElEICkge1xuXHRcdFx0Ly8gU2luY2UgYHdyYXBwZXJgIGlzIGJlaW5nIGNhbGxlZCBmb3IgdGhlIGZpcnN0IHRpbWUgYW5kXG5cdFx0XHQvLyBgZGVib3VuY2VNb2RlYCBpcyB0cnVlIChhdCBiZWdpbiksIGV4ZWN1dGUgYGNhbGxiYWNrYC5cblx0XHRcdGV4ZWMoKTtcblx0XHR9XG5cblx0XHQvLyBDbGVhciBhbnkgZXhpc3RpbmcgdGltZW91dC5cblx0XHRpZiAoIHRpbWVvdXRJRCApIHtcblx0XHRcdGNsZWFyVGltZW91dCh0aW1lb3V0SUQpO1xuXHRcdH1cblxuXHRcdGlmICggZGVib3VuY2VNb2RlID09PSB1bmRlZmluZWQgJiYgZWxhcHNlZCA+IGRlbGF5ICkge1xuXHRcdFx0Ly8gSW4gdGhyb3R0bGUgbW9kZSwgaWYgYGRlbGF5YCB0aW1lIGhhcyBiZWVuIGV4Y2VlZGVkLCBleGVjdXRlXG5cdFx0XHQvLyBgY2FsbGJhY2tgLlxuXHRcdFx0ZXhlYygpO1xuXG5cdFx0fSBlbHNlIGlmICggbm9UcmFpbGluZyAhPT0gdHJ1ZSApIHtcblx0XHRcdC8vIEluIHRyYWlsaW5nIHRocm90dGxlIG1vZGUsIHNpbmNlIGBkZWxheWAgdGltZSBoYXMgbm90IGJlZW5cblx0XHRcdC8vIGV4Y2VlZGVkLCBzY2hlZHVsZSBgY2FsbGJhY2tgIHRvIGV4ZWN1dGUgYGRlbGF5YCBtcyBhZnRlciBtb3N0XG5cdFx0XHQvLyByZWNlbnQgZXhlY3V0aW9uLlxuXHRcdFx0Ly9cblx0XHRcdC8vIElmIGBkZWJvdW5jZU1vZGVgIGlzIHRydWUgKGF0IGJlZ2luKSwgc2NoZWR1bGUgYGNsZWFyYCB0byBleGVjdXRlXG5cdFx0XHQvLyBhZnRlciBgZGVsYXlgIG1zLlxuXHRcdFx0Ly9cblx0XHRcdC8vIElmIGBkZWJvdW5jZU1vZGVgIGlzIGZhbHNlIChhdCBlbmQpLCBzY2hlZHVsZSBgY2FsbGJhY2tgIHRvXG5cdFx0XHQvLyBleGVjdXRlIGFmdGVyIGBkZWxheWAgbXMuXG5cdFx0XHR0aW1lb3V0SUQgPSBzZXRUaW1lb3V0KGRlYm91bmNlTW9kZSA/IGNsZWFyIDogZXhlYywgZGVib3VuY2VNb2RlID09PSB1bmRlZmluZWQgPyBkZWxheSAtIGVsYXBzZWQgOiBkZWxheSk7XG5cdFx0fVxuXG5cdH1cblxuXHQvLyBSZXR1cm4gdGhlIHdyYXBwZXIgZnVuY3Rpb24uXG5cdHJldHVybiB3cmFwcGVyO1xuXG59O1xuIiwiLyoqXG4gKiBIZWxwZXIgZnVuY3Rpb24gdG8gYWRkIGV2ZW50TGlzdGVuZXJzIHRvIGB3aW5kb3dgXG4gKiBAbW9kdWxlICBhZGRFdmVudExpc3RlbmVyc1xuICogQHByaXZhdGVcbiAqIEBhdXRob3IgIEdyZWdvciBBZGFtcyAgPGdyZWdAcGl4ZWxhc3MuY29tPlxuICovXG5cbi8qKlxuICogVGhyb3R0bGUgZXhlY3V0aW9uIG9mIGEgZnVuY3Rpb24uIEVzcGVjaWFsbHkgdXNlZnVsIGZvciByYXRlIGxpbWl0aW5nXG4gKiBleGVjdXRpb24gb2YgaGFuZGxlcnMgb24gZXZlbnRzIGxpa2UgcmVzaXplIGFuZCBzY3JvbGwuXG4gKiBAYXV0aG9yIG5pa3N5XG4gKiBAcHJpdmF0ZVxuICogQHR5cGUge0Z1bmN0aW9ufVxuICogQGNvbnN0XG4gKiBAcGFyYW0gIHtOdW1iZXJ9IGRlbGF5IC0gQSB6ZXJvLW9yLWdyZWF0ZXIgZGVsYXkgaW4gbWlsbGlzZWNvbmRzLiBGb3IgZXZlbnQgY2FsbGJhY2tzLCB2YWx1ZXMgYXJvdW5kIDEwMCBvciAyNTBcbiAqICAgICAgICAgICAgICAgICAgICAgICAgKG9yIGV2ZW4gaGlnaGVyKSBhcmUgbW9zdCB1c2VmdWwuXG4gKiBAcGFyYW0gIHtCb29sZWFufSBub1RyYWlsaW5nIC0gT3B0aW9uYWwsIGRlZmF1bHRzIHRvIGZhbHNlLiBJZiBub1RyYWlsaW5nIGlzIHRydWUsIGNhbGxiYWNrIHdpbGwgb25seSBleGVjdXRlIGV2ZXJ5XG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGBkZWxheWAgbWlsbGlzZWNvbmRzIHdoaWxlIHRoZSB0aHJvdHRsZWQtZnVuY3Rpb24gaXMgYmVpbmcgY2FsbGVkLiBJZiBub1RyYWlsaW5nIGlzIGZhbHNlXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yIHVuc3BlY2lmaWVkLCBjYWxsYmFjayB3aWxsIGJlIGV4ZWN1dGVkIG9uZSBmaW5hbCB0aW1lIGFmdGVyIHRoZSBsYXN0IHRocm90dGxlZC1mdW5jdGlvbiBjYWxsLlxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoQWZ0ZXIgdGhlIHRocm90dGxlZC1mdW5jdGlvbiBoYXMgbm90IGJlZW4gY2FsbGVkIGZvciBgZGVsYXlgIG1pbGxpc2Vjb25kcywgdGhlIGludGVybmFsIGNvdW50ZXIgaXMgcmVzZXQpXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gY2FsbGJhY2sgLSBBIGZ1bmN0aW9uIHRvIGJlIGV4ZWN1dGVkIGFmdGVyIGRlbGF5IG1pbGxpc2Vjb25kcy4gVGhlIGB0aGlzYCBjb250ZXh0IGFuZCBhbGwgYXJndW1lbnRzIGFyZSBwYXNzZWRcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdWdoLCBhcy1pcywgdG8gYGNhbGxiYWNrYCB3aGVuIHRoZSB0aHJvdHRsZWQtZnVuY3Rpb24gaXMgZXhlY3V0ZWQuXG4gKiBAcGFyYW0gIHtCb29sZWFufSBkZWJvdW5jZU1vZGUgLSBJZiBgZGVib3VuY2VNb2RlYCBpcyB0cnVlIChhdCBiZWdpbiksIHNjaGVkdWxlIGBjbGVhcmAgdG8gZXhlY3V0ZSBhZnRlciBgZGVsYXlgIG1zLlxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIElmIGBkZWJvdW5jZU1vZGVgIGlzIGZhbHNlIChhdCBlbmQpLCBzY2hlZHVsZSBgY2FsbGJhY2tgIHRvIGV4ZWN1dGUgYWZ0ZXIgYGRlbGF5YCBtcy5cbiAqXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn0gQSBuZXcsIHRocm90dGxlZCwgZnVuY3Rpb24uXG4gKi9cbmltcG9ydCB0aHJvdHRsZSBmcm9tICd0aHJvdHRsZS1kZWJvdW5jZS90aHJvdHRsZSdcblxuLyoqXG4gKiBhZGQgdGhlIHByb3BwZXIgZXZlbnQgbGlzdGVuZXIgdG8gYHdpbmRvd2BcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBjb25zdFxuICogQHByaXZhdGVcbiAqIEBwYXJhbSAge1N0cmluZ30gZXZlbnQgLSBldmVudCB0byBsaXN0ZW4gdG9cbiAqIEBwYXJhbSAge09iamVjdH0gaGFuZGxlciAtIHRoZSBoYW5kbGVyIG9iamVjdCBjb250YWluaW5nIHRoZSB0aHJvdHRsZWQgYW5kIHVudGhyb3R0bGVkIGhhbmRsZXJzXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gaGFuZGxlci5kZWZhdWx0IC0gZGVmYXVsdCBoYW5kbGVyXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gaGFuZGxlci50aHJvdHRsZSAtIHRocm90dGxlZCBoYW5kbGVyXG4gKiBAcGFyYW0gIHtOdW1iZXJ9IGRlbGF5IC0gdGhyb3R0bGUgaW4gbWlsbGlzZWNvbmRzXG4gKiBAcmV0dXJuIHtPYmplY3R9IHJldHVybnMgdGhlIG5ldyBoYW5kbGVyc1xuICovXG5jb25zdCBhZGRFdmVudExpc3RlbmVycyA9IChldmVudCwgaGFuZGxlciwgZGVsYXkgPSAwKSA9PiB7XG4gIGlmIChkZWxheSA+IDApIHtcbiAgICBoYW5kbGVyLnRocm90dGxlID0gdGhyb3R0bGUoZGVsYXksIChlKSA9PiBoYW5kbGVyLmRlZmF1bHQoZSkpXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoZXZlbnQsIGhhbmRsZXIudGhyb3R0bGUpXG4gIH0gZWxzZSB7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoZXZlbnQsIGhhbmRsZXIuZGVmYXVsdClcbiAgfVxuICByZXR1cm4gaGFuZGxlclxufVxuXG5leHBvcnQgZGVmYXVsdCBhZGRFdmVudExpc3RlbmVyc1xuIiwiLyoqXG4gKiBIZWxwZXIgZnVuY3Rpb24gdG8gZGV0ZXJtaW5lIHRoZSBoYW5kbGVyIHR5cGVcbiAqIEBtb2R1bGUgIGNoZWNrTGltaXRcbiAqIEBwcml2YXRlXG4gKiBAYXV0aG9yICBHcmVnb3IgQWRhbXMgIDxncmVnQHBpeGVsYXNzLmNvbT5cbiAqL1xuXG4vKipcbiAqIGNoZWNrIGxlbmd0aCB2cyBsaW1pdCBhbmQgcmV0dXJucyAgZWl0aGVyIGAwYCwgYDFgIG9yIGAyYFxuICogd2hlcmU6XG4gKiAtIDAgPSBub25lXG4gKiAtIDEgPSBkZWZhdWx0XG4gKiAtIDIgPSB0aHJvdHRsZWRcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBjb25zdFxuICogQHByaXZhdGVcbiAqIEBwYXJhbSAge051bWJlcn0gbGVuZ3RoIC0gbGVuZ3RoIG9mIGFycmF5XG4gKiBAcGFyYW0gIHtOdW1iZXJ9IGxpbWl0IC0gbGltaXQgb2YgaXRlbXMgaW4gYXJyYXlcbiAqIEByZXR1cm4ge051bWJlcn0gcmV0dXJucyBhIG51bWJlciAob25lIG9mIGBbMCwxLDJdYClcbiAqL1xuY29uc3QgY2hlY2tMaW1pdCA9IChsZW5ndGgsIGxpbWl0KSA9PiAhbGVuZ3RoID8gMCA6IGxlbmd0aCA+IGxpbWl0ID8gMiA6IDFcblxuZXhwb3J0IGRlZmF1bHQgY2hlY2tMaW1pdFxuIiwiLyoqXG4gKiBBcnJheSBvZiBldmVudHMgdG8gcmV1c2VcbiAqIEBtb2R1bGUgIEVWRU5UU1xuICogQHByaXZhdGVcbiAqIEBhdXRob3IgIEdyZWdvciBBZGFtcyAgPGdyZWdAcGl4ZWxhc3MuY29tPlxuICovXG5cbi8qKlxuICogYWxsIGV2ZW50cyBzdG9yZWQgZm9yIHF1aWNrIGFjY2Vzc1xuICogQHR5cGUge0FycmF5fVxuICogQGNvbnN0XG4gKiBAcHJpdmF0ZVxuICovXG5jb25zdCBFVkVOVFMgPSBbJ3Njcm9sbCcsICdyZXNpemUnLCAnbW91c2V3aGVlbCcsICdtb3VzZW1vdmUnLCAnbW91c2V1cCcsICd0b3VjaG1vdmUnLCAndG91Y2hlbmQnXVxuXG5leHBvcnQgZGVmYXVsdCBFVkVOVFNcbiIsIi8qKlxuICogT25lTGlzdGVuZXIgYXR0ZW1wdHMgdG8gZ2F0aGVyIGEgY29sbGVjdGlvbiBvZiBoYW5kbGVycyB0byBiZSBleGVjdXRlZCBkdXJpbmcgYW4gZXZlbnQuXG4gKiBVc3VhbGx5IGRldmVsb3BlcnMgcmVnaXN0ZXIgdGhlIGhhbmRsZXJzIGRpcmVjdGx5IG9uIHRoZSBgd2luZG93YCB3aGljaCBjYW4gY2F1c2UgbWVtb3J5IGxlYWtzIGR1ZSB0byB1bnJlbW92ZWQgYGV2ZW50TGlzdGVuZXJzYC5cbiAqXG4gKiBXaGVuIGEgbG90IG9mIGhhbmRsZXJzIGFyZSBhY3RpdmUgdGhlIHBlcmZvcm1hbmNlIG9mIHRoZSBhcHAgb3IgcGFnZSBtaWdodCBiZSByZWR1Y2VkLlxuICogT25lIExpc3RlbmVyIHRocm90dGxlcyB0aGUgZXZlbnRzIGdsb2JhbGx5IHRoZXJlZm9yZSB0aGUgaGFuZGxyZXJzIHdpbGwgYXV0b21hdGljYWxseSBiZSB0aHJvdHRsZWQuXG4gKiBUaGlzIGlzIGJhc2VkIG9uIGEgaGFuZGxlcjpsaW1pdCByYXRpbyB3aGljaCBjYW4gYmUgZGVmaW5lZCB3aGVuIGNyZWF0aW5nIGFuIGluc3RhbmNlIG9yIGNoYW5nZWQgZHVyaW5nIHJ1bnRpbWUuXG4gKlxuICogQG1vZHVsZSAgb25lLWxpc3RlbmVyXG4gKlxuICogQGF1dGhvciAgR3JlZ29yIEFkYW1zICA8Z3JlZ0BwaXhlbGFzcy5jb20+XG4gKiBAYXV0aG9yICBKYW4gTmlja2xhc1xuICogQGV4YW1wbGVcbiAqIGltcG9ydCBPbmVMaXN0ZW5lciBmcm9tICdvbmUtbGlzdGVuZXInXG4gKiBjb25zdCBvbmUgPSBuZXcgT25lTGlzdGVuZXIoe1xuICogICBsaW1pdDogNixcbiAqICAgdGhyb3R0bGU6IDIwMFxuICogfSlcbiAqIGNvbnN0IHtyZXF1ZXN0RXZlbnRMaXN0ZW5lciwgY2FuY2VsRXZlbnRMaXN0ZW5lcn0gPSBvbmVcbiAqL1xuXG4vKipcbiAqIFRocm90dGxlIGV4ZWN1dGlvbiBvZiBhIGZ1bmN0aW9uLiBFc3BlY2lhbGx5IHVzZWZ1bCBmb3IgcmF0ZSBsaW1pdGluZ1xuICogZXhlY3V0aW9uIG9mIGhhbmRsZXJzIG9uIGV2ZW50cyBsaWtlIHJlc2l6ZSBhbmQgc2Nyb2xsLlxuICogQGF1dGhvciBuaWtzeVxuICogQHByaXZhdGVcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBjb25zdFxuICogQHBhcmFtICB7TnVtYmVyfSBkZWxheSAtIEEgemVyby1vci1ncmVhdGVyIGRlbGF5IGluIG1pbGxpc2Vjb25kcy4gRm9yIGV2ZW50IGNhbGxiYWNrcywgdmFsdWVzIGFyb3VuZCAxMDAgb3IgMjUwXG4gKiAgICAgICAgICAgICAgICAgICAgICAgIChvciBldmVuIGhpZ2hlcikgYXJlIG1vc3QgdXNlZnVsLlxuICogQHBhcmFtICB7Qm9vbGVhbn0gbm9UcmFpbGluZyAtIE9wdGlvbmFsLCBkZWZhdWx0cyB0byBmYWxzZS4gSWYgbm9UcmFpbGluZyBpcyB0cnVlLCBjYWxsYmFjayB3aWxsIG9ubHkgZXhlY3V0ZSBldmVyeVxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICBgZGVsYXlgIG1pbGxpc2Vjb25kcyB3aGlsZSB0aGUgdGhyb3R0bGVkLWZ1bmN0aW9uIGlzIGJlaW5nIGNhbGxlZC4gSWYgbm9UcmFpbGluZyBpcyBmYWxzZVxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvciB1bnNwZWNpZmllZCwgY2FsbGJhY2sgd2lsbCBiZSBleGVjdXRlZCBvbmUgZmluYWwgdGltZSBhZnRlciB0aGUgbGFzdCB0aHJvdHRsZWQtZnVuY3Rpb24gY2FsbC5cbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKEFmdGVyIHRoZSB0aHJvdHRsZWQtZnVuY3Rpb24gaGFzIG5vdCBiZWVuIGNhbGxlZCBmb3IgYGRlbGF5YCBtaWxsaXNlY29uZHMsIHRoZSBpbnRlcm5hbCBjb3VudGVyIGlzIHJlc2V0KVxuICogQHBhcmFtICB7RnVuY3Rpb259IGNhbGxiYWNrIC0gQSBmdW5jdGlvbiB0byBiZSBleGVjdXRlZCBhZnRlciBkZWxheSBtaWxsaXNlY29uZHMuIFRoZSBgdGhpc2AgY29udGV4dCBhbmQgYWxsIGFyZ3VtZW50cyBhcmUgcGFzc2VkXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3VnaCwgYXMtaXMsIHRvIGBjYWxsYmFja2Agd2hlbiB0aGUgdGhyb3R0bGVkLWZ1bmN0aW9uIGlzIGV4ZWN1dGVkLlxuICogQHBhcmFtICB7Qm9vbGVhbn0gZGVib3VuY2VNb2RlIC0gSWYgYGRlYm91bmNlTW9kZWAgaXMgdHJ1ZSAoYXQgYmVnaW4pLCBzY2hlZHVsZSBgY2xlYXJgIHRvIGV4ZWN1dGUgYWZ0ZXIgYGRlbGF5YCBtcy5cbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZiBgZGVib3VuY2VNb2RlYCBpcyBmYWxzZSAoYXQgZW5kKSwgc2NoZWR1bGUgYGNhbGxiYWNrYCB0byBleGVjdXRlIGFmdGVyIGBkZWxheWAgbXMuXG4gKlxuICogQHJldHVybiB7RnVuY3Rpb259IEEgbmV3LCB0aHJvdHRsZWQsIGZ1bmN0aW9uLlxuICovXG5pbXBvcnQgdGhyb3R0bGUgZnJvbSAndGhyb3R0bGUtZGVib3VuY2UvdGhyb3R0bGUnXG5cbmltcG9ydCBjaGVja0xpbWl0IGZyb20gJy4vY2hlY2stbGltaXQnXG5pbXBvcnQgcmVtb3ZlRXZlbnRMaXN0ZW5lcnMgZnJvbSAnLi9yZW1vdmUtZXZlbnQtbGlzdGVuZXJzJ1xuaW1wb3J0IGFkZEV2ZW50TGlzdGVuZXJzIGZyb20gJy4vYWRkLWV2ZW50LWxpc3RlbmVycydcbmltcG9ydCBFVkVOVFMgZnJvbSAnLi9ldmVudHMnXG5cbi8qKlxuICogQ3JlYXRlcyBhbiBpbnN0YW5jZSBvZiBPbmVMaXN0ZW5lci4gQW4gaW5zdGFuY2Ugd2lsbCBoYW5kbGUgZWFjaCBldmVudCBzZXBlcmF0ZWx5IGJ1dCB1c2VzIGEgZ2xvYmFsIHRocm90dGxlIGFuZCBsaW1pdC5cbiAqIElmIGBzY3JvbGxgIGlzIHRocm90dGxlZCBhbGwgcmVxdWVzdGVkIGhhbmRsZXJzIHdpbGwgcmV0dXJuIFwidGhyb3R0bGVkXCIsIHdoaWxlIGFsbCBgcmVzaXplYCBoYW5kbGVycyByZWx5IG9uIHRoZWlyIG93biBsaW1pdDpsZW5ndGggcmF0aW8uXG4gKiBZb3UgY2FuIGNyZWF0ZSBtdWx0aXBsZSBpbnN0YW5jZXMgdG8gYWxsb3cgYWRkaW5nIHBlcm1hbmVudGx5IFwidGhyb3R0bGVkXCIgb3IgXCJ1bnRocm90dGxlZFwiIGBldmVudExpc3RlbmVyc2AgYXNpZGUgb2YgeW91ciBub3JtYWwgaW5zdGFuY2UuXG4gKlxuICogTGV0J3Mgc2F5IHlvdSBoYXZlIGEgcGFyYWxsYXggcGFnZSB3aGljaCByZWxpZXMgb24gcmFwaWQgdHJhY2tpbmc6IFRoaXMgd291bGQgYmUgcmVxdWVzdGVkIG9uIGFuIHVudGhyb3R0bGVkIGluc3RhbmNlLlxuICogSW4gY2FzZSB5b3Ugd2FudCB0byByZXBvc2l0aW9uIGFuIGVsZW1lbnQgb24gcmVzaXplIGl0IG1pZ2h0IGJlIHdpc2UgdG8gcmVxdWVzdCBvbiB0aGUgdGhyb3R0bGVkIGluc3RhbmNlXG4gKiBBbGwgaGFuZGxlcnMgYXJlIHdyYXBwZWQgaW4gYSBgcmVxdWVzdEFuaW1hdGlvbkZyYW1lYCB0byB1c2UgdGhlIGJyb3dzZXJzIGRlZmF1bHQgdGhyb3R0bGluZyBtZWNoYW5pc20uXG4gKiBAdHlwZSB7Q2xhc3N9XG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyAtIGN1c3RvbSBvcHRpb25zXG4gKiBAcGFyYW0ge051bWJlcn0gb3B0aW9ucy5saW1pdCAtIGlmIGhhbmRsZXJzLmxlbmd0aCBleGNlZWRzIHRoaXMgdmFsdWUgdGhlIGdsb2JhbCBldmVudExpc3RlbmVyIHdpbGwgYmUgdGhyb3R0bGVkXG4gKiBAcGFyYW0ge051bWJlcn0gb3B0aW9ucy50aHJvdHRsZSAtIGlmIHRocm90dGxlZCBoYW5kbGVycyB3aWxsIG9ubHkgYmUgY2FsbGVkIGV2ZXJ5IGBOIG1zYFxuICogQHJldHVybiB7bW9kdWxlOm9uZS1saXN0ZW5lcn5PbmVMaXN0ZW5lcn0gcmV0dXJucyBPbmVMaXN0ZW5lciBpbnN0YW5jZVxuICpcbiAqIEBleGFtcGxlXG4gKiAvLyBkZWZhdWx0XG4gKiBjb25zdCBvbmUgPSBuZXcgT25lTGlzdGVuZXIoKVxuICogLy8gY3VzdG9tXG4gKiBjb25zdCBvbmUgPSBuZXcgT25lTGlzdGVuZXIoe1xuICogICBsaW1pdDogMyxcbiAqICAgdGhyb3R0bGU6IDUwMFxuICogfSlcbiAqL1xuY2xhc3MgT25lTGlzdGVuZXIge1xuICAvKipcbiAgICogYnVpbGRzIGFuZCByZXR1cm5zIGFuIGluc3RhbmNlIG9mIE9uZUxpc3RlbmVyXG4gICAqL1xuICBjb25zdHJ1Y3RvciAob3B0aW9ucykge1xuICAgIC8qKlxuICAgICAqIGFkZCBzb21lIG9wdGlvbnMgdG8gY3VzdG9taXplIHRoZSBiZWhhdmlvclxuICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAYWxpYXMgb3B0aW9uc1xuICAgICAqIEBtZW1iZXJvZiBtb2R1bGU6b25lLWxpc3RlbmVyfk9uZUxpc3RlbmVyXG4gICAgICogQHByb3BlcnR5IHtOdW1iZXJ9IGxpbWl0IC0gaWYgaGFuZGxlcnMubGVuZ3RoIGV4Y2VlZHMgdGhpcyB2YWx1ZSB0aGUgZ2xvYmFsIGV2ZW50TGlzdGVuZXIgd2lsbCBiZSB0aHJvdHRsZWRcbiAgICAgKiBAcHJvcGVydHkge051bWJlcn0gdGhyb3R0bGUgLSBpZiB0aHJvdHRsZWQgaGFuZGxlcnMgd2lsbCBvbmx5IGJlIGNhbGxlZCBldmVyeSBgTiBtc2BcbiAgICAgKi9cbiAgICB0aGlzLm9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHtcbiAgICAgIGxpbWl0OiAxMCxcbiAgICAgIHRocm90dGxlOiAxMDBcbiAgICB9LCBvcHRpb25zKVxuXG4gICAgdGhpcy5oYW5kbGVTY3JvbGwgPSB0aGlzLmhhbmRsZVNjcm9sbC5iaW5kKHRoaXMpXG4gICAgdGhpcy5oYW5kbGVSZXNpemUgPSB0aGlzLmhhbmRsZVJlc2l6ZS5iaW5kKHRoaXMpXG4gICAgdGhpcy5oYW5kbGVNb3VzZXdoZWVsID0gdGhpcy5oYW5kbGVNb3VzZXdoZWVsLmJpbmQodGhpcylcbiAgICB0aGlzLmhhbmRsZU1vdXNlbW92ZSA9IHRoaXMuaGFuZGxlTW91c2Vtb3ZlLmJpbmQodGhpcylcbiAgICB0aGlzLmhhbmRsZU1vdXNldXAgPSB0aGlzLmhhbmRsZU1vdXNldXAuYmluZCh0aGlzKVxuICAgIHRoaXMuaGFuZGxlVG91Y2htb3ZlID0gdGhpcy5oYW5kbGVUb3VjaG1vdmUuYmluZCh0aGlzKVxuICAgIHRoaXMuaGFuZGxlVG91Y2hlbmQgPSB0aGlzLmhhbmRsZVRvdWNoZW5kLmJpbmQodGhpcylcbiAgICB0aGlzLnJlcXVlc3RFdmVudExpc3RlbmVyID0gdGhpcy5yZXF1ZXN0RXZlbnRMaXN0ZW5lci5iaW5kKHRoaXMpXG4gICAgdGhpcy5jYW5jZWxFdmVudExpc3RlbmVyID0gdGhpcy5jYW5jZWxFdmVudExpc3RlbmVyLmJpbmQodGhpcylcblxuICAgIHRoaXMuaW5pdCgpXG4gIH1cblxuICAvKipcbiAgICogaW5pdGlhbGl6ZSBPbmVMaXN0ZW5lci5cbiAgICogVGhpcyBtZXRob2QgaXMgb25seSBjYWxsZWQgdG8gc2V0IHVwIHRvIGNvbmZpZ3VyZVxuICAgKiB0aGUgaW5zdGFuY2UuXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBpbml0ICgpIHtcbiAgICAvKipcbiAgICAgKiBnbG9iYWwgY29sbGVjdGlvbiBvZiBsaXN0ZW5lcnNcbiAgICAgKiBUaGlzIG9iamVjdCBpcyB0aGUgaW50ZXJuYWwgc3RvcmUgZm9yIHRoZSBldmVudCBsaXN0ZW5lcnNcbiAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAqIEBwcml2YXRlXG4gICAgICogQG1lbWJlcm9mIG1vZHVsZTpvbmUtbGlzdGVuZXJ+T25lTGlzdGVuZXJcbiAgICAgKiBAcHJvcGVydHkge0FycmF5fSBzY3JvbGwgLSBjb2xsZWN0aW9uIG9mIHNjcm9sbCBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7QXJyYXl9IHJlc2l6ZSAtIGNvbGxlY3Rpb24gb2YgcmVzaXplIGhhbmRsZXJzXG4gICAgICogQHByb3BlcnR5IHtBcnJheX0gbW91c2V3aGVlbCAtIGNvbGxlY3Rpb24gb2YgbW91c2V3aGVlbCBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7QXJyYXl9IG1vdXNlbW92ZSAtIGNvbGxlY3Rpb24gb2YgbW91c2Vtb3ZlIGhhbmRsZXJzXG4gICAgICogQHByb3BlcnR5IHtBcnJheX0gbW91c2V1cCAtIGNvbGxlY3Rpb24gb2YgbW91c2V1cCBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7QXJyYXl9IHRvdWNobW92ZSAtIGNvbGxlY3Rpb24gb2YgdG91Y2htb3ZlIGhhbmRsZXJzXG4gICAgICogQHByb3BlcnR5IHtBcnJheX0gdG91Y2hlbmQgLSBjb2xsZWN0aW9uIG9mIHRvdWNoZW5kIGhhbmRsZXJzXG4gICAgICovXG4gICAgdGhpcy5ldmVudExpc3RlbmVycyA9IHtcbiAgICAgIHNjcm9sbDogW10sXG4gICAgICByZXNpemU6IFtdLFxuICAgICAgbW91c2V3aGVlbDogW10sXG4gICAgICBtb3VzZW1vdmU6IFtdLFxuICAgICAgbW91c2V1cDogW10sXG4gICAgICB0b3VjaG1vdmU6IFtdLFxuICAgICAgdG91Y2hlbmQ6IFtdXG4gICAgfVxuXG4gICAgLypcbiAgICAgKiB0aGUgc3RhdGUgb2JqZWN0IHdpbGwgaGVscCB0byBjaGVjayBpZiB3ZSBuZWVkIHRvIHVwZGF0ZSB0aGUgZXZlbnRMaXN0ZW5lclxuICAgICAqIGR1ZSB0byBzd2l0Y2hpbmcgZnJvbSBvciB0byBhIHRocm90dGxlZCBsaXN0ZW5lci5cbiAgICAgKiB3aGVyZTpcbiAgICAgKiAtIDAgPSBub25lXG4gICAgICogLSAxID0gZGVmYXVsdFxuICAgICAqIC0gMiA9IHRocm90dGxlZFxuICAgICAqIEBwcml2YXRlXG4gICAgICogQG1lbWJlcm9mIG1vZHVsZTpvbmUtbGlzdGVuZXJ+T25lTGlzdGVuZXJcbiAgICAgKiBAcHJvcGVydHkge051bWJlcn0gc2Nyb2xsIC0gb25lIG9mIGBbMCwxLDJdYFxuICAgICAqIEBwcm9wZXJ0eSB7TnVtYmVyfSByZXNpemUgLSBvbmUgb2YgYFswLDEsMl1gXG4gICAgICogQHByb3BlcnR5IHtOdW1iZXJ9IG1vdXNld2hlZWwgLSBvbmUgb2YgYFswLDEsMl1gXG4gICAgICogQHByb3BlcnR5IHtOdW1iZXJ9IG1vdXNlbW92ZSAtIG9uZSBvZiBgWzAsMSwyXWBcbiAgICAgKiBAcHJvcGVydHkge051bWJlcn0gbW91c2V1cCAtIG9uZSBvZiBgWzAsMSwyXWBcbiAgICAgKiBAcHJvcGVydHkge051bWJlcn0gdG91Y2htb3ZlIC0gb25lIG9mIGBbMCwxLDJdYFxuICAgICAqIEBwcm9wZXJ0eSB7TnVtYmVyfSB0b3VjaGVuZCAtIG9uZSBvZiBgWzAsMSwyXWBcbiAgICAgKi9cbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgc2Nyb2xsOiAwLFxuICAgICAgcmVzaXplOiAwLFxuICAgICAgbW91c2V3aGVlbDogMCxcbiAgICAgIG1vdXNlbW92ZTogMCxcbiAgICAgIG1vdXNldXA6IDAsXG4gICAgICB0b3VjaG1vdmU6IDAsXG4gICAgICB0b3VjaGVuZDogMFxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIGFkZCBoYW5kbGVycyBmb3IgYWxsIGV2ZW50c1xuICAgICAqIHByb3ZpZGVzIGEgdGhyb3R0bGVkIGFuZCBkZWZhdWx0IGhhbmRsZXJcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEBtZW1iZXJvZiBtb2R1bGU6b25lLWxpc3RlbmVyfk9uZUxpc3RlbmVyXG4gICAgICogQHByb3BlcnR5IHtPYmplY3R9IHNjcm9sbCAtIHNjcm9sbCBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IHNjcm9sbC5kZWZhdWx0IC0gZGVmYXVsdCBzY3JvbGwgaGFuZGxlclxuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IHNjcm9sbC50aHJvdHRsZWQgLSB0aHJvdHRsZWQgc2Nyb2xsIGhhbmRsZXJcbiAgICAgKiBAcHJvcGVydHkge09iamVjdH0gcmVzaXplIC0gcmVzaXplIGhhbmRsZXJzXG4gICAgICogQHByb3BlcnR5IHtGdW5jdGlvbn0gcmVzaXplLmRlZmF1bHQgLSBkZWZhdWx0IHJlc2l6ZSBoYW5kbGVyXG4gICAgICogQHByb3BlcnR5IHtGdW5jdGlvbn0gcmVzaXplLnRocm90dGxlZCAtIHRocm90dGxlZCByZXNpemUgaGFuZGxlclxuICAgICAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBtb3VzZXdoZWVsIC0gbW91c2V3aGVlbCBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IG1vdXNld2hlZWwuZGVmYXVsdCAtIGRlZmF1bHQgbW91c2V3aGVlbCBoYW5kbGVyXG4gICAgICogQHByb3BlcnR5IHtGdW5jdGlvbn0gbW91c2V3aGVlbC50aHJvdHRsZWQgLSB0aHJvdHRsZWQgbW91c2V3aGVlbCBoYW5kbGVyXG4gICAgICogQHByb3BlcnR5IHtPYmplY3R9IG1vdXNlbW92ZSAtIG1vdXNlbW92ZSBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IG1vdXNlbW92ZS5kZWZhdWx0IC0gZGVmYXVsdCBtb3VzZW1vdmUgaGFuZGxlclxuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IG1vdXNlbW92ZS50aHJvdHRsZWQgLSB0aHJvdHRsZWQgbW91c2Vtb3ZlIGhhbmRsZXJcbiAgICAgKiBAcHJvcGVydHkge09iamVjdH0gbW91c2V1cCAtIG1vdXNldXAgaGFuZGxlcnNcbiAgICAgKiBAcHJvcGVydHkge0Z1bmN0aW9ufSBtb3VzZXVwLmRlZmF1bHQgLSBkZWZhdWx0IG1vdXNldXAgaGFuZGxlclxuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IG1vdXNldXAudGhyb3R0bGVkIC0gZGVmYXVsdCBtb3VzZXVwIGhhbmRsZXIgKGFsd2F5cyB1bnRocm90dGxlZClcbiAgICAgKiBAcHJvcGVydHkge09iamVjdH0gdG91Y2htb3ZlIC0gdG91Y2htb3ZlIGhhbmRsZXJzXG4gICAgICogQHByb3BlcnR5IHtGdW5jdGlvbn0gdG91Y2htb3ZlLmRlZmF1bHQgLSBkZWZhdWx0IHRvdWNobW92ZSBoYW5kbGVyXG4gICAgICogQHByb3BlcnR5IHtGdW5jdGlvbn0gdG91Y2htb3ZlLnRocm90dGxlZCAtIHRocm90dGxlZCB0b3VjaG1vdmUgaGFuZGxlclxuICAgICAqIEBwcm9wZXJ0eSB7T2JqZWN0fSB0b3VjaHVwIC0gdG91Y2h1cCBoYW5kbGVyc1xuICAgICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IHRvdWNoZW5kLmRlZmF1bHQgLSBkZWZhdWx0IHRvdWNoZW5kIGhhbmRsZXJcbiAgICAgKiBAcHJvcGVydHkge0Z1bmN0aW9ufSB0b3VjaGVuZC50aHJvdHRsZWQgLSBkZWZhdWx0IHRvdWNoZW5kIGhhbmRsZXIgKGFsd2F5cyB1bnRocm90dGxlZClcbiAgICAgKi9cbiAgICB0aGlzLmhhbmRsZXJzID0ge1xuICAgICAgc2Nyb2xsOiB7XG4gICAgICAgIGRlZmF1bHQ6IHRoaXMuaGFuZGxlU2Nyb2xsLFxuICAgICAgICB0aHJvdHRsZWQ6IHRocm90dGxlKHRoaXMub3B0aW9ucy50aHJvdHRsZSwgKGUpID0+IHRoaXMuaGFuZGxlU2Nyb2xsKGUpKVxuICAgICAgfSxcbiAgICAgIHJlc2l6ZToge1xuICAgICAgICBkZWZhdWx0OiB0aGlzLmhhbmRsZVJlc2l6ZSxcbiAgICAgICAgdGhyb3R0bGVkOiB0aHJvdHRsZSh0aGlzLm9wdGlvbnMudGhyb3R0bGUsIChlKSA9PiB0aGlzLmhhbmRsZVJlc2l6ZShlKSlcbiAgICAgIH0sXG4gICAgICBtb3VzZXdoZWVsOiB7XG4gICAgICAgIGRlZmF1bHQ6IHRoaXMuaGFuZGxlTW91c2V3aGVlbCxcbiAgICAgICAgdGhyb3R0bGVkOiB0aHJvdHRsZSh0aGlzLm9wdGlvbnMudGhyb3R0bGUsIChlKSA9PiB0aGlzLmhhbmRsZU1vdXNld2hlZWwoZSkpXG4gICAgICB9LFxuICAgICAgbW91c2Vtb3ZlOiB7XG4gICAgICAgIGRlZmF1bHQ6IHRoaXMuaGFuZGxlTW91c2Vtb3ZlLFxuICAgICAgICB0aHJvdHRsZWQ6IHRocm90dGxlKHRoaXMub3B0aW9ucy50aHJvdHRsZSwgKGUpID0+IHRoaXMuaGFuZGxlTW91c2Vtb3ZlKGUpKVxuICAgICAgfSxcbiAgICAgIG1vdXNldXA6IHtcbiAgICAgICAgZGVmYXVsdDogdGhpcy5oYW5kbGVNb3VzZXVwLFxuICAgICAgICAvLyBuZXZlciB0aHJvdHRsZSBtb3VzZXVwXG4gICAgICAgIHRocm90dGxlZDogdGhpcy5oYW5kbGVNb3VzZXVwXG4gICAgICB9LFxuICAgICAgdG91Y2htb3ZlOiB7XG4gICAgICAgIGRlZmF1bHQ6IHRoaXMuaGFuZGxlVG91Y2htb3ZlLFxuICAgICAgICB0aHJvdHRsZWQ6IHRocm90dGxlKHRoaXMub3B0aW9ucy50aHJvdHRsZSwgKGUpID0+IHRoaXMuaGFuZGxlVG91Y2htb3ZlKGUpKVxuICAgICAgfSxcbiAgICAgIHRvdWNoZW5kOiB7XG4gICAgICAgIGRlZmF1bHQ6IHRoaXMuaGFuZGxlVG91Y2hlbmQsXG4gICAgICAgIC8vIG5ldmVyIHRocm90dGxlIHRvdWNodXBcbiAgICAgICAgdGhyb3R0bGVkOiB0aGlzLmhhbmRsZVRvdWNoZW5kXG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5jaGVja0xpbWl0QWxsKHRydWUpXG4gIH1cblxuICAvKipcbiAgICogY2hlY2sgbGltaXQgb2YgaGFuZGxlcnMgdnMgc2V0dGluZ3MgYW5kIHVwZGF0ZSBsaXN0ZW5lcnMgdG8gdGhyb3R0bGUgd2hlbiByZWFjaGVkXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSAge1N0cmluZ30gZXZlbnQgLSBoYW5kbGVycyB0byBjaGVja1xuICAgKiBAcGFyYW0gIHtCb29sZWFufSBmb3JjZSAtIGZvcmNlIHVwZGF0ZVxuICAgKi9cbiAgY2hlY2tMaW1pdCAoZXZlbnQsIGZvcmNlKSB7XG4gICAgY29uc3QgY2FjaGUgPSB0aGlzLnN0YXRlW2V2ZW50XVxuICAgIC8vIHNldCB0aHJvdHRsZWQgb3IgZGVmYXVsdCBoYW5kbGVycyBkZXBlbmRpbmcgb24gdGhlIGxpbWl0Omxlbmd0aCByYXRpb1xuICAgIC8vIGlmIG5vIGhhbmRsZXJzIGV4aXN0IGRvbid0IGFkZCBhbiBldmVudExpc3RlbmVyXG4gICAgY29uc3QgdXBkYXRlID0gY2hlY2tMaW1pdCh0aGlzLmV2ZW50TGlzdGVuZXJzW2V2ZW50XS5sZW5ndGgsIHRoaXMub3B0aW9ucy5saW1pdClcbiAgICAvLyBvbmx5IGFwcGx5IGlmIHRoZSBzdGF0ZSBoYXMgY2hhbmdlZFxuICAgIGlmICh1cGRhdGUgIT09IGNhY2hlIHx8IGZvcmNlKSB7XG4gICAgICB0aGlzLnN0YXRlW2V2ZW50XSA9IHVwZGF0ZVxuICAgICAgcmVtb3ZlRXZlbnRMaXN0ZW5lcnMoZXZlbnQsIHRoaXMuaGFuZGxlcnNbZXZlbnRdKVxuICAgICAgc3dpdGNoICh1cGRhdGUpIHtcbiAgICAgICAgY2FzZSAyOlxuICAgICAgICAgIHRoaXMuaGFuZGxlcnNbZXZlbnRdID0gYWRkRXZlbnRMaXN0ZW5lcnMoZXZlbnQsIHRoaXMuaGFuZGxlcnNbZXZlbnRdLCB0aGlzLm9wdGlvbnMudGhyb3R0bGUpXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgY2FzZSAxOlxuICAgICAgICAgIHRoaXMuaGFuZGxlcnNbZXZlbnRdID0gYWRkRXZlbnRMaXN0ZW5lcnMoZXZlbnQsIHRoaXMuaGFuZGxlcnNbZXZlbnRdKVxuICAgICAgICAgIGJyZWFrXG4gICAgICAgIGNhc2UgMDpcbiAgICAgICAgICBicmVha1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignc3RhdGUgc2hvdWxkIGJlIG9uIG9mIFswLDEsMl0gYnV0IHdhczogJyArIHRoaXMuc3RhdGVbZXZlbnRdKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKiBzaG9ydGN1dCB0byBjaGVjayBhbGwgaGFuZGxlcnMgdGhhdCBhcmUgY3VycmVudGx5IHJlcXVlc3RlZFxuICAgKiBAcHJpdmF0ZVxuICAgKiBAcGFyYW0ge0Jvb2xlYW59IGZvcmNlIC0gZm9yY2VzIGFuIHVwZGF0ZSAodXNlZnVsIHdoZW4gaW5pdGlhbGl6aW5nIG9yIHNpbWlsYXIpXG4gICAqL1xuICBjaGVja0xpbWl0QWxsIChmb3JjZSkge1xuICAgIEVWRU5UUy5mb3JFYWNoKChldmVudCkgPT4gdGhpcy5jaGVja0xpbWl0KGV2ZW50LCBmb3JjZSkpXG4gIH1cblxuICAvKipcbiAgICogcmVxdWVzdCBhbiBldmVudExpc3RlbmVyXG4gICAqIHN0b3JlcyB0aGUgaGFuZGxlc3IgaW4gdGhlIGludGVybmFsIHN0b3JhZ2UgYW5kIHJldHVybnN0IGEgY2FuY2VsIGZ1bmN0aW9uXG4gICAqIEBwdWJsaWNcbiAgICogQHBhcmFtICB7U3RyaW5nfSBldmVudCAtIG5hbWUgb2YgdGhlIGV2ZW50IHRvIHJlcXVlc3RcbiAgICogQHBhcmFtICB7RnVuY3Rpb259IGhhbmRsZXIgLSBkZWZhdWx0IGV2ZW50TGlzdGVuZXIgaGFuZGxlclxuICAgKiBAcmV0dXJuIHtGdW5jdGlvbn0gcmV0dXJucyBhIGZ1bmN0aW9uIHdoaWNoIHdpbGwgY2FuY2VsIHRoZSBldmVudExpc3RlbmVyXG4gICAqXG4gICAqIEBleGFtcGxlXG4gICAqIGNvbnN0IG9uZSA9IG5ldyBPbmVMaXN0ZW5lcigpXG4gICAqIGNvbnN0IHtyZXF1ZXN0RXZlbnRMaXN0ZW5lcn0gPSBvbmVcbiAgICogLy8gc2ltcGxlIGxpc3RlbmVyXG4gICAqIHJlcXVlc3RFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBoYW5kbGVTY3JvbGwpXG4gICAqIC8vIHdpdGggY2FuY2VsXG4gICAqIGNvbnN0IGNhbmNlbFNjcm9sbCA9IHJlcXVlc3RFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBoYW5kbGVTY3JvbGwpXG4gICAqIC8vIGNhbGwgY2FuY2VsRXZlbnRsaXN0ZW5lciB2aWEgY2FuY2VsU2Nyb2xsKClcbiAgICogY2FuY2VsU2Nyb2xsKCkgLy8gbGlzdGVuZXIgY2FuY2VsZWRcbiAgICovXG4gIHJlcXVlc3RFdmVudExpc3RlbmVyIChldmVudCwgaGFuZGxlcikge1xuICAgIGlmICghdGhpcy5ldmVudExpc3RlbmVycy5oYXNPd25Qcm9wZXJ0eShldmVudCkpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignVW5rb3duIGV2ZW50ICcgKyBldmVudClcbiAgICB9XG4gICAgdGhpcy5ldmVudExpc3RlbmVyc1tldmVudF0ucHVzaChoYW5kbGVyKVxuICAgIHRoaXMuY2hlY2tMaW1pdChldmVudClcbiAgICByZXR1cm4gKCkgPT4gdGhpcy5jYW5jZWxFdmVudExpc3RlbmVyKGV2ZW50LCBoYW5kbGVyKVxuICB9XG5cbiAgLyoqXG4gICAqIGNhbmNlbHMgYW4gZXZlbnRMaXN0ZW5lclxuICAgKiBsb29rcyBmb3IgdGhlIGhhbmRsZXIgYW5kIHJlbW92ZXMgaXQgZnJvbSB0aGUgbGlzdFxuICAgKiBAcHVibGljXG4gICAqIEBwYXJhbSAge1N0cmluZ30gZXZlbnQgLSBuYW1lIG9mIHRoZSBldmVudCB0byBjYW5jZWxcbiAgICogQHBhcmFtICB7RnVuY3Rpb259IGhhbmRsZXIgLSBoYW5kbGVyIHRvIGJlIHJlbW92ZWRcbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogY29uc3Qgb25lID0gbmV3IE9uZUxpc3RlbmVyKClcbiAgICogY29uc3Qge2NhbmNlbEV2ZW50TGlzdGVuZXJ9ID0gb25lXG4gICAqIGNhbmNlbEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGhhbmRsZVNjcm9sbClcbiAgICovXG4gIGNhbmNlbEV2ZW50TGlzdGVuZXIgKGV2ZW50LCBoYW5kbGVyKSB7XG4gICAgaWYgKCF0aGlzLmV2ZW50TGlzdGVuZXJzLmhhc093blByb3BlcnR5KGV2ZW50KSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdVbmtvd24gZXZlbnQgJyArIGV2ZW50KVxuICAgIH1cbiAgICBjb25zdCBpbmRleCA9IHRoaXMuZXZlbnRMaXN0ZW5lcnNbZXZlbnRdLmluZGV4T2YoaGFuZGxlcilcbiAgICAvLyBTa2lwIGlmIHRoZSBoYW5kbGVyIGRvZXNuJ3QgZXhpc3RcbiAgICBpZiAoaW5kZXggPT09IC0xKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG4gICAgLy8gdXBkYXRlIGhhbmRsZXJzIGFuZCByZWJ1aWxkIGxpc3RlbmVyc1xuICAgIHRoaXMuZXZlbnRMaXN0ZW5lcnNbZXZlbnRdLnNwbGljZShpbmRleCwgMSlcbiAgICB0aGlzLmNoZWNrTGltaXQoZXZlbnQpXG4gIH1cblxuICAvKipcbiAgICogbmFtZWQgc2Nyb2xsIGhhbmRsZXIgdG8gdXNlIGluIGBhZGRFdmVudExpc3RlbmVyYCBhbmQgYHJlbW92ZUV2ZW50TGlzdGVuZXJgXG4gICAqIGNhbGxzIGFsbCBoYW5kbGVycyB3cmFwcGVkIGluIGEgYHJlcXVlc3RBbmltYXRpb25GcmFtZWBcbiAgICogQHByaXZhdGVcbiAgICogQHBhcmFtICB7RXZlbnR9IGUgLSBzY3JvbGwgZXZlbnRcbiAgICovXG4gIGhhbmRsZVNjcm9sbCAoZSkge1xuICAgIHRoaXMuZXZlbnRMaXN0ZW5lcnMuc2Nyb2xsLmZvckVhY2goKGhhbmRsZXIpID0+IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKCkgPT4gaGFuZGxlcihlKSkpXG4gIH1cblxuICAvKipcbiAgICogbmFtZWQgcmVzaXplIGhhbmRsZXIgdG8gdXNlIGluIGBhZGRFdmVudExpc3RlbmVyYCBhbmQgYHJlbW92ZUV2ZW50TGlzdGVuZXJgXG4gICAqIGNhbGxzIGFsbCBoYW5kbGVycyB3cmFwcGVkIGluIGEgYHJlcXVlc3RBbmltYXRpb25GcmFtZWBcbiAgICogQHByaXZhdGVcbiAgICogQHBhcmFtICB7RXZlbnR9IGUgLSByZXNpemUgZXZlbnRcbiAgICovXG4gIGhhbmRsZVJlc2l6ZSAoZSkge1xuICAgIHRoaXMuZXZlbnRMaXN0ZW5lcnMucmVzaXplLmZvckVhY2goKGhhbmRsZXIpID0+IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKCkgPT4gaGFuZGxlcihlKSkpXG4gIH1cblxuICAvKipcbiAgICogbmFtZWQgbW91c2V3aGVlbCBoYW5kbGVyIHRvIHVzZSBpbiBgYWRkRXZlbnRMaXN0ZW5lcmAgYW5kIGByZW1vdmVFdmVudExpc3RlbmVyYFxuICAgKiBjYWxscyBhbGwgaGFuZGxlcnMgd3JhcHBlZCBpbiBhIGByZXF1ZXN0QW5pbWF0aW9uRnJhbWVgXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSAge0V2ZW50fSBlIC0gbW91c2V3aGVlbCBldmVudFxuICAgKi9cbiAgaGFuZGxlTW91c2V3aGVlbCAoZSkge1xuICAgIHRoaXMuZXZlbnRMaXN0ZW5lcnMubW91c2V3aGVlbC5mb3JFYWNoKChoYW5kbGVyKSA9PiB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKCgpID0+IGhhbmRsZXIoZSkpKVxuICB9XG5cbiAgLyoqXG4gICAqIG5hbWVkIG1vdXNlbW92ZSBoYW5kbGVyIHRvIHVzZSBpbiBgYWRkRXZlbnRMaXN0ZW5lcmAgYW5kIGByZW1vdmVFdmVudExpc3RlbmVyYFxuICAgKiBjYWxscyBhbGwgaGFuZGxlcnMgd3JhcHBlZCBpbiBhIGByZXF1ZXN0QW5pbWF0aW9uRnJhbWVgXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSAge0V2ZW50fSBlIC0gbW91c2Vtb3ZlIGV2ZW50XG4gICAqL1xuICBoYW5kbGVNb3VzZW1vdmUgKGUpIHtcbiAgICB0aGlzLmV2ZW50TGlzdGVuZXJzLm1vdXNlbW92ZS5mb3JFYWNoKChoYW5kbGVyKSA9PiB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKCgpID0+IGhhbmRsZXIoZSkpKVxuICB9XG5cbiAgLyoqXG4gICAqIG5hbWVkIG1vdXNldXAgaGFuZGxlciB0byB1c2UgaW4gYGFkZEV2ZW50TGlzdGVuZXJgIGFuZCBgcmVtb3ZlRXZlbnRMaXN0ZW5lcmBcbiAgICogY2FsbHMgYWxsIGhhbmRsZXJzXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSAge0V2ZW50fSBlIC0gbW91c2V1cCBldmVudFxuICAgKi9cbiAgaGFuZGxlTW91c2V1cCAoZSkge1xuICAgIHRoaXMuZXZlbnRMaXN0ZW5lcnMubW91c2V1cC5mb3JFYWNoKChoYW5kbGVyKSA9PiBoYW5kbGVyKGUpKVxuICB9XG5cbiAgLyoqXG4gICAqIG5hbWVkIHRvdWNobW92ZSBoYW5kbGVyIHRvIHVzZSBpbiBgYWRkRXZlbnRMaXN0ZW5lcmAgYW5kIGByZW1vdmVFdmVudExpc3RlbmVyYFxuICAgKiBjYWxscyBhbGwgaGFuZGxlcnMgd3JhcHBlZCBpbiBhIGByZXF1ZXN0QW5pbWF0aW9uRnJhbWVgXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSAge0V2ZW50fSBlIC0gdG91Y2htb3ZlIGV2ZW50XG4gICAqL1xuICBoYW5kbGVUb3VjaG1vdmUgKGUpIHtcbiAgICB0aGlzLmV2ZW50TGlzdGVuZXJzLnRvdWNobW92ZS5mb3JFYWNoKChoYW5kbGVyKSA9PiB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKCgpID0+IGhhbmRsZXIoZSkpKVxuICB9XG5cbiAgLyoqXG4gICAqIG5hbWVkIHRvdWNoZW5kIGhhbmRsZXIgdG8gdXNlIGluIGBhZGRFdmVudExpc3RlbmVyYCBhbmQgYHJlbW92ZUV2ZW50TGlzdGVuZXJgXG4gICAqIGNhbGxzIGFsbCBoYW5kbGVyc1xuICAgKiBAcHJpdmF0ZVxuICAgKiBAcGFyYW0gIHtFdmVudH0gZSAtIHRvdWNoZW5kIGV2ZW50XG4gICAqL1xuICBoYW5kbGVUb3VjaGVuZCAoZSkge1xuICAgIHRoaXMuZXZlbnRMaXN0ZW5lcnMudG91Y2hlbmQuZm9yRWFjaCgoaGFuZGxlcikgPT4gaGFuZGxlcihlKSlcbiAgfVxuXG4gIC8qKlxuICAgKiByZXR1cm4gYWxsIGV2ZW50TGlzdGVuZXJzXG4gICAqIEB0eXBlIHtHZXR0ZXJ9XG4gICAqIEBhbGlhcyBnZXQgZGVidWdcbiAgICogQG5hbWUgZ2V0IGRlYnVnXG4gICAqIEBtZW1iZXJvZiBtb2R1bGU6b25lLWxpc3RlbmVyfk9uZUxpc3RlbmVyXG4gICAqIEByZXR1cm4ge09iamVjdH0gdGhlIGludGVybmFsIHN0b3JlXG4gICAqXG4gICAqIEBleGFtcGxlXG4gICAqIGNvbnN0IG9uZSA9IG5ldyBPbmVMaXN0ZW5lcigpXG4gICAqIGNvbnNvbGUubG9nKG9uZS5kZWJ1ZylcbiAgICovXG4gIGdldCBkZWJ1ZyAoKSB7XG4gICAgcmV0dXJuIHRoaXMuZXZlbnRMaXN0ZW5lcnNcbiAgfVxuXG4gIC8qKlxuICAgKiBzZXQgbGltaXQgb2YgaGFuZGxlcnNcbiAgICogQHR5cGUge1NldHRlcn1cbiAgICogQGFsaWFzIHNldCBsaW1pdFxuICAgKiBAbmFtZSBzZXQgbGltaXRcbiAgICogQG1lbWJlcm9mIG1vZHVsZTpvbmUtbGlzdGVuZXJ+T25lTGlzdGVuZXJcbiAgICogQHBhcmFtICB7TnVtYmVyfSBbdmFsdWVdIG1vZGlmaWVzIHRoZSBsaW1pdFxuICAgKlxuICAgKiBAZXhhbXBsZVxuICAgKiBjb25zdCBvbmUgPSBuZXcgT25lTGlzdGVuZXIoKVxuICAgKiBvbmUubGltaXQgPSA0XG4gICAqL1xuICBzZXQgbGltaXQgKHZhbHVlKSB7XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgIHRoaXMub3B0aW9ucy5saW1pdCA9IHZhbHVlXG4gICAgICB0aGlzLmNoZWNrTGltaXRBbGwodHJ1ZSlcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCd2YWx1ZSBzaG91bGQgYmUgb2YgdHlwZSBcIm51bWJlclwiIGluc3RlYWQgZ290ICcgKyB0eXBlb2YgdmFsdWUpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGdldCBsaW1pdCBvZiBoYW5kbGVyc1xuICAgKiBAdHlwZSB7R2V0dGVyfVxuICAgKiBAYWxpYXMgZ2V0IGxpbWl0XG4gICAqIEBuYW1lIGdldCBsaW1pdFxuICAgKiBAbWVtYmVyb2YgbW9kdWxlOm9uZS1saXN0ZW5lcn5PbmVMaXN0ZW5lclxuICAgKiBAcmV0dXJuIHtOdW1iZXJ9IHJldHVybnMgdGhlIGN1cnJlbnQgbGltaXRcbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogY29uc3Qgb25lID0gbmV3IE9uZUxpc3RlbmVyKClcbiAgICogbGV0IGxpbWl0ID0gb25lLmxpbWl0XG4gICAqL1xuICBnZXQgbGltaXQgKCkge1xuICAgIHJldHVybiB0aGlzLm9wdGlvbnMubGltaXRcbiAgfVxuXG4gIC8qKlxuICAgKiBzZXQgdGhyb3R0bGUgb2YgaGFuZGxlcnNcbiAgICogQHR5cGUge1NldHRlcn1cbiAgICogQGFsaWFzIHNldCB0aHJvdHRsZVxuICAgKiBAbmFtZSBzZXQgdGhyb3R0bGVcbiAgICogQG1lbWJlcm9mIG1vZHVsZTpvbmUtbGlzdGVuZXJ+T25lTGlzdGVuZXJcbiAgICogQHBhcmFtICB7TnVtYmVyfSBbdmFsdWVdIG1vZGlmaWVzIHRoZSB0aHJvdHRsZVxuICAgKlxuICAgKiBAZXhhbXBsZVxuICAgKiBjb25zdCBvbmUgPSBuZXcgT25lTGlzdGVuZXIoKVxuICAgKiBvbmUudGhyb3R0bGUgPSA1MDBcbiAgICovXG4gIHNldCB0aHJvdHRsZSAodmFsdWUpIHtcbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnbnVtYmVyJykge1xuICAgICAgdGhpcy5vcHRpb25zLnRocm90dGxlID0gdmFsdWVcbiAgICAgIHRoaXMuY2hlY2tMaW1pdEFsbCh0cnVlKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ3ZhbHVlIHNob3VsZCBiZSBvZiB0eXBlIFwibnVtYmVyXCIgaW5zdGVhZCBnb3QgJyArIHR5cGVvZiB2YWx1ZSlcbiAgICB9XG4gIH1cblxuICAvKipcbiAgKiBnZXQgdGhyb3R0bGUgb2YgaGFuZGxlcnNcbiAgKiBAdHlwZSB7R2V0dGVyfVxuICAqIEBhbGlhcyBnZXQgdGhyb3R0bGVcbiAgKiBAbmFtZSBnZXQgdGhyb3R0bGVcbiAgKiBAbWVtYmVyb2YgbW9kdWxlOm9uZS1saXN0ZW5lcn5PbmVMaXN0ZW5lclxuICAqIEByZXR1cm4ge051bWJlcn0gcmV0dXJucyB0aGUgY3VycmVudCB0aHJvdHRsZVxuICAqXG4gICogQGV4YW1wbGVcbiAgKiBjb25zdCBvbmUgPSBuZXcgT25lTGlzdGVuZXIoKVxuICAqIGxldCB0aHJvdHRsZSA9IG9uZS50aHJvdHRsZVxuICAqL1xuICBnZXQgdGhyb3R0bGUgKCkge1xuICAgIHJldHVybiB0aGlzLm9wdGlvbnMudGhyb3R0bGVcbiAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IE9uZUxpc3RlbmVyXG4iLCIvKipcbiAqIEhlbHBlciBmdW5jdGlvbiB0byByZW1vdmUgZXZlbnRMaXN0ZW5lcnMgZnJvbSBgd2luZG93YFxuICogQG1vZHVsZSAgcmVtb3ZlRXZlbnRMaXN0ZW5lcnNcbiAqIEBwcml2YXRlXG4gKiBAYXV0aG9yICBHcmVnb3IgQWRhbXMgIDxncmVnQHBpeGVsYXNzLmNvbT5cbiAqL1xuXG4vKipcbiAqIHJlbW92ZSBhbGwgbGlzdGVuZXJzIHRoYXQgYXJlIHBvdGVudGlhbGx5IG9uIGB3aW5kb3dgXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAY29uc3RcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0gIHtTdHJpbmd9IGV2ZW50ICAgZXZlbnQgdG8gZm9yZ2V0XG4gKiBAcGFyYW0gIHtPYmplY3R9IGhhbmRsZXIgLSB0aGUgaGFuZGxlciBvYmplY3QgY29udGFpbmluZyB0aGUgdGhyb3R0bGVkIGFuZCB1bnRocm90dGxlZCBoYW5kbGVyc1xuICogQHBhcmFtICB7RnVuY3Rpb259IGhhbmRsZXIuZGVmYXVsdCAtIGRlZmF1bHQgaGFuZGxlclxuICogQHBhcmFtICB7RnVuY3Rpb259IGhhbmRsZXIudGhyb3R0bGUgLSB0aHJvdHRsZWQgaGFuZGxlclxuICovXG5jb25zdCByZW1vdmVFdmVudExpc3RlbmVycyA9IChldmVudCwgaGFuZGxlcikgPT4ge1xuICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudCwgaGFuZGxlci50aHJvdHRsZSlcbiAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoZXZlbnQsIGhhbmRsZXIuZGVmYXVsdClcbn1cblxuZXhwb3J0IGRlZmF1bHQgcmVtb3ZlRXZlbnRMaXN0ZW5lcnNcbiJdfQ==
