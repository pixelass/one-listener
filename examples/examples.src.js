import OneListener from '../src/'

const one = new OneListener({
  limit: 6,
  throttle: 200
})

const never = new OneListener({
  limit: Infinity
})

const always = new OneListener({
  limit: 0,
  throttle: 100
})

const {requestEventListener, cancelEventListener} = one

// request mousemove
const stopMoveTracking = requestEventListener('mousemove', (e) => {
  console.log({
    x: e.pageX,
    y: e.pageY
  })
})

let move = true
// request scroll
// and cancel mousemove on condition
const trackScroll = (e) => {
  console.log(window.scrollY)
  if (window.scrollY > 100) {
    if (move) {
      console.log('mousemove canceled')
      stopMoveTracking()
      move = false
    }
  }
}

const noop = () => {}

requestEventListener('scroll', trackScroll)

for (let i = 0; i < 8; i++) {
  requestEventListener('scroll', noop)
}

for (let i = 0; i < 3; i++) {
  requestEventListener('resize', noop)
}

requestEventListener('resize', (e) => {
  console.log('resizing window')
})

setTimeout(() => {
  one.throttle = 1000
  one.limit = 30
  console.log('events throttled')
  setTimeout(() => {
    cancelEventListener('scroll', trackScroll)
    one.throttle = 5000
    one.limit = 2
    console.log('events throttled less')
    console.log('scrolling canceled')
    console.log(`try resizing the browser (${one.throttle}ms throttle)`)
  }, 3000)
}, 3000)

console.log('limit: ', one.limit)
console.log('throttle: ', one.throttle)
console.log('Debugger: \n', one.debug)

never.requestEventListener('scroll', (e) => {
  console.log('never throttled')
})

always.requestEventListener('scroll', (e) => {
  console.log(`always throttled by ${always.throttle}ms`)
})
