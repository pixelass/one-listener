# one-listener


big project?  
many contributors?  

too many event listeners on `window`?

This library aims to gather all handlers in one listener. This way the number of listeners are reduced.  
Performance intense listeners are wrapped in a `requestAnimationFrame`.  
If a certain limit of handlers is reached events will be throttled.
Everything is configurable. You can even create your own module and lock certain features.

available listeners

* scroll
* resize
* mousewheel
* mousemove
* mouseup (default behavior)
* touchmove
* touchend (default behavior)

## API

A detailed documentation can be found [here](http://pixelass.github.io/one-listener)  
You can also take a look at the [examples](https://github.com/pixelass/one-listener/blob/master/examples/examples.src.js)


## Pride

[![npm](https://img.shields.io/npm/v/one-listener.svg)](https://www.npmjs.com/package/one-listener)
[![Coveralls branch](https://img.shields.io/coveralls/pixelass/one-listener.svg)](https://coveralls.io/github/pixelass/one-listener)
[![Bithound Code](https://img.shields.io/bithound/code/github/pixelass/one-listener.svg)](https://www.bithound.io/github/pixelass/one-listener)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg)](https://github.com/conventional-changelog/standard-version)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)  
[![Travis](https://img.shields.io/travis/pixelass/one-listener.svg)](https://travis-ci.org/pixelass/one-listener)
[![David](https://img.shields.io/david/pixelass/one-listener.svg)](https://david-dm.org/pixelass/one-listener)
[![David](https://img.shields.io/david/dev/pixelass/one-listener.svg)](https://david-dm.org/pixelass/one-listener#info=devDependencies&view=table)  
[![GitHub license](https://img.shields.io/github/license/pixelass/one-listener.svg)](https://github.com/pixelass/one-listener/blob/master/LICENSE)
[![GitHub issues](https://img.shields.io/github/issues/pixelass/one-listener.svg)](https://github.com/pixelass/one-listener/issues)
[![GitHub forks](https://img.shields.io/github/forks/pixelass/one-listener.svg)](https://github.com/pixelass/one-listener/network)
[![GitHub stars](https://img.shields.io/github/stars/pixelass/one-listener.svg)](https://github.com/pixelass/one-listener/stargazers)  

## installation
```bash
npm install one-listener
```

## Examples

Look at the examples folder for a detailed example

### simple

```js
import OneListener from 'one-listener';

const one = new OneListener({
    limit: 6,
    throttle: 200
});
const {requestEventListener, cancelEventListener} = one;
```

### API
```js
import OneListener from 'one-listener';
const one = new OneListener()
const {requestEventListener, cancelEventListener} = one;


// request mousemove 
const stopMoveTracking = requestEventListener('mousemove', (e) => {
    console.log({
        x: e.pageX,
        y: e.pageY
    });
});

// request scroll
// and cancel mousemove on condition 
const trackScroll = (e) => {
    console.log(window.scrollY);
    if (window.scrollY > 100) {
        stopMoveTracking();
    }
};

requestEventListener('scroll', trackScroll);

// remove scroll tracking after 5 seconds
setTimeout(() => {
    cancelEventListener('scroll', trackScroll);
}, 5000);
```

### Setters
```js
import OneListener from 'one-listener';

const one = new OneListener();
one.limit = 20
one.throttle = 200
```

### Getters
```js
import OneListener from 'one-listener';

const one = new OneListener();
console.log(one.limit);
console.log(one.throttle);
console.log(one.debug);
```



### locked instances

```js
import OneListener from 'one-listener';

// create locked instances (listeners.js)
const o = new OneListener({
    limit: 6,
    throttle: 200
});
const n = new OneListener({
    limit: Infinity
});
const a = new OneListener({
    limit: 0,
    throttle: 100
});

export const one = {
  requestEventListener: o.requestEventListener,
  cancelEventListener: o.cancelEventListener
}
export const always = {
  requestEventListener: a.requestEventListener,
  cancelEventListener: a.cancelEventListener
}
export const never = {
  requestEventListener: n.requestEventListener,
  cancelEventListener: n.cancelEventListener
}


// then import (somefile.js)

import {one,never,always} from './listeners';
const {requestEventListener, cancelEventListener} = one;

//requestEventListener() || one.requestEventListener()
//always.requestEventListener()
//never.requestEventListener()

```
