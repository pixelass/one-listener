/**
 * Helper function to add eventListeners to `window`
 * @module  addEventListeners
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

import throttle from 'throttle-debounce/throttle'

/**
 * add the propper event listener to `window`
 * @type {Function}
 * @const
 * @private
 * @param  {String} event - event to listen to
 * @param  {Object} handler - the handler object containing the throttled and unthrottled handlers
 * @param  {Function} handler.default - default handler
 * @param  {Function} handler.throttled - throttled handler
 * @param  {Number} delay - throttle in milliseconds
 * @return {Object} returns the new handlers
 */
const addEventListeners = (event, handler, delay = 0) => {
  if (delay > 0) {
    /* istanbul ignore next */
    handler.throttled = throttle(delay, (e) => handler.default(e))
    window.addEventListener(event, handler.throttled)
  } else {
    window.addEventListener(event, handler.default)
  }
  return handler
}

export default addEventListeners
