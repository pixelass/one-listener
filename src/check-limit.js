/**
 * Helper function to determine the handler type
 * @module  checkLimit
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * check length vs limit and returns  either `0`, `1` or `2`
 * where:
 * - 0 = none
 * - 1 = default
 * - 2 = throttled
 * @type {Function}
 * @const
 * @private
 * @param  {Number} length - length of array
 * @param  {Number} limit - limit of items in array
 * @return {Number} returns a number (one of `[0,1,2]`)
 */
const checkLimit = (length, limit) => !length ? 0 : length > limit ? 2 : 1

export default checkLimit
