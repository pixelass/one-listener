/**
 * Array of events to reuse
 * @module  EVENTS
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * all events stored for quick access
 * @type {Array}
 * @const
 * @private
 */
const EVENTS = ['scroll', 'resize', 'mousewheel', 'mousemove', 'mouseup', 'touchmove', 'touchend']

export default EVENTS
