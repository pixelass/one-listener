/**
 * Helper function to remove eventListeners from `window`
 * @module  removeEventListeners
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * remove all listeners that are potentially on `window`
 * @type {Function}
 * @const
 * @private
 * @param  {String} event   event to forget
 * @param  {Object} handler - the handler object containing the throttled and unthrottled handlers
 * @param  {Function} handler.default - default handler
 * @param  {Function} handler.throttled - throttled handler
 */
const removeEventListeners = (event, handler) => {
  window.removeEventListener(event, handler.throttled)
  window.removeEventListener(event, handler.default)
}

export default removeEventListeners
