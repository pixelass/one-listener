/* global describe, it */

import {expect} from 'chai'
import addEventListeners from '../add-event-listeners'

describe('addEventListeners', () => {
  it('can have a throttled function', () => {
    const check = addEventListeners('resize', () => {}, 10)
    expect(check).to.be.a('function')
  })
  it('can have a default function', () => {
    const check = addEventListeners('resize', () => {}, 0)
    expect(check).to.be.a('function')
    check()
  })
})
