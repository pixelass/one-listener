/* global describe, it */

import {expect} from 'chai'
import checkLimit from '../check-limit'

describe('checkLimit', () => {
  it('should return 0 if length === 0', () => {
    const check = checkLimit(0, 100)
    expect(check).to.equal(0)
  })
  it('should return 1 if length <= limit but length > 0', () => {
    let check = checkLimit(1, 100)
    expect(check).to.equal(1)
    check = checkLimit(100, 100)
    expect(check).to.equal(1)
  })
  it('should return 2 if length > limit', () => {
    const check = checkLimit(101, 100)
    expect(check).to.equal(2)
  })
})
