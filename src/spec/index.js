/* global describe, it, window */
global.window = global.window || {
  addEventListener: (event, handler, useCapture) => {
  },
  removeEventListener: (event, handler, useCapture) => {
  },
  requestAnimationFrame: (fn) => {
    fn()
  },
  cancelAnimationFrame: (fn) => {
  }
}

import {expect} from 'chai'
import OneListener from '..'

const noop = () => {}

describe('OneListener', () => {
  noop()
  it('throws an error for unknow events', () => {
    const one = new OneListener()
    const fn1 = () => {
      one.requestEventListener('foobar', noop)
    }
    const fn2 = () => {
      one.cancelEventListener('foobar', noop)
    }
    expect(fn1).to.throw(Error)
    expect(fn2).to.throw(Error)
  })
  it('skips unknown handlers', () => {
    const one = new OneListener()
    const fn = () => {}
    const cancel = () => {
      return one.cancelEventListener('resize', fn)
    }
    one.requestEventListener('resize', noop)
    one.handleResize()
    expect(cancel()).to.be.undefined
  })
  // test existence of methods
  it('has a requestEventListener method', () => {
    const one = new OneListener()
    expect(one.requestEventListener).to.be.a('function')
  })
  it('has a cancelEventListener method', () => {
    const one = new OneListener()
    expect(one.cancelEventListener).to.be.a('function')
  })
  it('has a handleScroll method', () => {
    const one = new OneListener()
    expect(one.handleScroll).to.be.a('function')
    const cancelListener = one.requestEventListener('scroll', noop)
    one.handleScroll()
    expect(one.handlers.scroll.throttled).to.be.a('function')
    one.handlers.scroll.throttled()
    expect(one.handlers.scroll.default).to.be.a('function')
    one.handlers.scroll.default()
    cancelListener()
  })
  it('has a handleResize method', () => {
    const one = new OneListener()
    expect(one.handleResize).to.be.a('function')
    one.requestEventListener('resize', noop)
    one.handleResize()
    expect(one.handlers.resize.throttled).to.be.a('function')
    one.handlers.resize.throttled()
    expect(one.handlers.resize.default).to.be.a('function')
    one.handlers.resize.default()
    one.cancelEventListener('resize', noop)
  })
  it('has a handleMousewheel method', () => {
    const one = new OneListener()
    expect(one.handleMousewheel).to.be.a('function')
    one.requestEventListener('mousewheel', noop)
    one.handleMousewheel()
    expect(one.handlers.mousewheel.throttled).to.be.a('function')
    one.handlers.mousewheel.throttled()
    expect(one.handlers.mousewheel.default).to.be.a('function')
    one.handlers.mousewheel.default()
    one.cancelEventListener('mousewheel', noop)
  })
  it('has a handleMousemove method', () => {
    const one = new OneListener()
    expect(one.handleMousemove).to.be.a('function')
    one.requestEventListener('mousemove', noop)
    one.handleMousemove()
    expect(one.handlers.mousemove.throttled).to.be.a('function')
    one.handlers.mousemove.throttled()
    expect(one.handlers.mousemove.default).to.be.a('function')
    one.handlers.mousemove.default()
    one.requestEventListener('mousemove', noop)
  })
  it('has a handleMouseup method', () => {
    const one = new OneListener()
    expect(one.handleMouseup).to.be.a('function')
    one.requestEventListener('mouseup', noop)
    one.handleMouseup()
    expect(one.handlers.mouseup.throttled).to.be.a('function')
    one.handlers.mouseup.throttled()
    expect(one.handlers.mouseup.default).to.be.a('function')
    one.handlers.mouseup.default()
    one.cancelEventListener('mouseup', noop)
  })
  it('has a handleTouchmove method', () => {
    const one = new OneListener()
    expect(one.handleTouchmove).to.be.a('function')
    one.requestEventListener('touchmove', noop)
    one.handleTouchmove()
    expect(one.handlers.touchmove.throttled).to.be.a('function')
    one.handlers.touchmove.throttled()
    expect(one.handlers.touchmove.default).to.be.a('function')
    one.handlers.touchmove.default()
    one.cancelEventListener('touchmove', noop)
  })
  it('has a handleTouchend method', () => {
    const one = new OneListener()
    expect(one.handleTouchend).to.be.a('function')
    one.requestEventListener('touchend', noop)
    one.handleTouchend()
    expect(one.handlers.touchend.throttled).to.be.a('function')
    one.handlers.touchend.throttled()
    expect(one.handlers.touchend.default).to.be.a('function')
    one.handlers.touchend.default()
    one.cancelEventListener('touchend', noop)
  })

  it('has throttles', () => {
    const one = new OneListener({limit: 0})
    one.requestEventListener('resize', noop)
    const fn = () => {
      one.checkLimit('foobar')
    }
    expect(fn).to.throw(Error)
  })

  describe('instances', () => {
    it('allows having different instances', () => {
      const one = new OneListener()
      const two = new OneListener()
      expect(one).not.to.equal(two)
    })
  })

  // test existence and functionality of getters

  describe('getters', () => {
    it('has a limit getter', () => {
      const one = new OneListener()
      expect(one.limit).to.be.a('number')
    })
    it('has a throttle getter', () => {
      const one = new OneListener()
      expect(one.throttle).to.be.a('number')
    })
    it('has a debug getter', () => {
      const one = new OneListener()
      expect(one.debug).to.be.a('object')
    })
  })

  // test existence and functionality of setters

  describe('setters', () => {
    it('has a limit setter', () => {
      const one = new OneListener()
      const limit = ~~(Math.random() * 40 + 1)
      one.limit = limit
      expect(one.limit).to.equal(limit)
      const fn = () => {
        one.limit = 'a'
      }
      expect(fn).to.throw(Error)
      expect(fn).to.throw(Error, /value should be of type "number" instead got string/)
    })
    it('has a throttle setter', () => {
      const one = new OneListener()
      const throttle = ~~(Math.random() * 1000 + 100)
      one.throttle = throttle
      expect(one.throttle).to.equal(throttle)
      const fn = () => {
        one.throttle = 'a'
      }
      expect(fn).to.throw(Error)
      expect(fn).to.throw(Error, /value should be of type "number" instead got string/)
    })
  })

  // test existence and functionality of debug

  describe('debug', () => {
    it('contains arrays', () => {
      const one = new OneListener()
      for (const key in one.debug) {
        expect(one.debug[key]).to.be.a('array')
      }
    })
    it('is a getter', () => {
      const one = new OneListener()
      expect(one.debug).to.be.a('object')
    })
  })

  describe('requestEventListener', () => {
    it('should return a function', () => {
      const one = new OneListener()
      const cancelHandler = one.requestEventListener('scroll', noop)
      expect(cancelHandler).to.be.a('function')
      cancelHandler()
    })
    it('should add a handler', () => {
      const one = new OneListener()
      const before = one.debug.scroll.length
      const cancelHandler = one.requestEventListener('scroll', noop)
      const after = one.debug.scroll.length
      expect(after).to.be.above(before)
      cancelHandler()
    })
    describe('cancelHandler', () => {
      it('should remove a handler', () => {
        const one = new OneListener()
        const before = one.debug.scroll.length
        const cancelHandler = one.requestEventListener('scroll', noop)
        cancelHandler()
        const after = one.debug.scroll.length
        expect(after).to.equal(before)
      })
      it('should cancel its own handler', () => {
        const one = new OneListener()
        const foo = () => noop()
        const bar = () => noop()
        const cancelFoo = one.requestEventListener('scroll', foo)
        const cancelBar = one.requestEventListener('scroll', bar)
        foo()
        bar()
        cancelFoo()
        const indexFoo = one.debug.scroll.indexOf(foo)
        let indexBar = one.debug.scroll.indexOf(bar)
        expect(indexFoo).to.equal(-1)
        expect(indexBar).to.be.above(-1)
        cancelBar()
        indexBar = one.debug.scroll.indexOf(bar)
        expect(indexBar).to.equal(-1)
      })
    })
  })

  describe('cancelEventListener', () => {
    it('should remove its handler', () => {
      const one = new OneListener()
      const handleScroll = () => noop()
      one.requestEventListener('scroll', handleScroll)
      let index = one.debug.scroll.indexOf(handleScroll)
      handleScroll()
      expect(index).to.be.above(-1)
      one.cancelEventListener('scroll', handleScroll)
      index = one.debug.scroll.indexOf(handleScroll)
      expect(index).to.equal(-1)
    })
  })
})
